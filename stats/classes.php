<?php	

#GESTORE DELLE PAGINE
class Page {
    var $namePage;

	public function __construct( $name ){
		$this->namePage = $name;
    }

	public function render( $values ){

		try{
			require_once( 'libs/Twig/Autoloader.php' );
			Twig_Autoloader::register();
			$loader   = new Twig_Loader_Filesystem( ROOT_DIR.'/templates' );
			$twig     = new Twig_Environment( $loader );
			$template = $twig->loadTemplate( $this->namePage );

			echo $template->render( $values );

		} catch( Exception $e ) {
			die( ERROR.": ".$e->getMessage() );
		}

	}
}


#GESTORE DELLE QUERY
class queryHandler {
	var $dbHandler;
	var $status;
	var $sth;

	public function __construct( $dbname ){
		try {
			$this->dbHandler = $this->db_connect( $dbname );
		}
		catch ( PDOException $e ) {
			$e = new Error(1, "error connecting to database", "database ".$dbname);
			$e->stampError();
		}
    }

	public function runQuery( $query, $array_parametri ){

		//CODICE OSCURATO PER MOTIVI DI SICUREZZA
		
	}

	public function getResult(){
		$result = $this->sth->fetchAll();
		return $result;
	}

	public function getFirstRow(){
		return $this->sth->fetch();
	}

	public function noError(){
		return $this->status;
	}

	public function searchError( $query ){
		if( !$this->status ){
			$e = new Error(2, "query error", "query: ".$query);
			$e->stampError();
		}
	}

	public function getResultJSON(){
		$result = $this->sth->fetchAll();
		return json_encode($result);
	}

	public function getNumRows(){
		$rows = $this->sth->fetch( PDO::FETCH_NUM );
		return $rows;
	}

	public function close(){
		$this->dbHandler = null;
	}

	//connessione al database
	public function db_connect( $dbname ){
		//CODICE OSCURATO PER MOTIVI DI SICUREZZA
	}
}

?>