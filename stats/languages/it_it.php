<?php

	// COMMON
	define("PROJECT_NAME", 	"Example");
	define("INDEX", 		"HomePage");
	define("HIDDEN", 		"Nascosto");
	define("VISIBLE", 		"Visibile");
	define("SUBMIT", 		"Invia");

	//mesi
	define("MONTH_1", 					"Gennaio");
	define("MONTH_2", 					"Febbraio");
	define("MONTH_3", 					"Marzo");
	define("MONTH_4", 					"Aprile");
	define("MONTH_5", 					"Maggio");
	define("MONTH_6", 					"Giugno");
	define("MONTH_7", 					"Luglio");
	define("MONTH_8", 					"Agosto");
	define("MONTH_9", 					"Settembre");
	define("MONTH_10", 					"Ottobre");
	define("MONTH_11", 					"Novebre");
	define("MONTH_12", 					"Dicembre");


	//regioni
	define("ABRUZZO", 					"Abruzzo");
	define("BASILICATA", 				"Basilicata");
	define("CALABRIA", 					"Calabria");
	define("CAMPANIA", 					"Campania");
	define("EMILIA_ROMAGNA", 			"Emilia-Romagna");
	define("FRIULI_VENEZIA_GIULIA", 	"Friuli-Venezia Giulia");
	define("LAZIO", 					"Lazio");
	define("LIGURIA", 					"Liguria");
	define("LOMBARDIA", 				"Lombardia");
	define("MARCHE", 					"Marche");
	define("MOLISE", 					"Molise");
	define("PIEMONTE", 					"Piemonte");
	define("PUGLIA", 					"Puglia");
	define("SARDEGNA", 					"Sardegna");
	define("SICILIA", 					"Sicilia");
	define("TOSCANA", 					"Toscana");
	define("TRENTINO_ALTO_ADIGE", 		"Trentino-Alto Adige");
	define("UMBRIA", 					"Umbria");
	define("VALLE_D_AOSTA", 			"Valle d'Aosta");
	define("VENETO", 					"Veneto");

	define("ALL_REGIONS", 				"Tutta italia");

	define("TITLE_GENERAL",				"Statistiche di utilizzo");
	define("DESCRIPTION_TABLE_GENERAL",	"Principali siti che utilizzano JADE e numero di visite ai singoli elementi");
	define("TABLE_COL_ONE_GENERAL",		"Sito");
	define("TABLE_COL_TWO_GENERAL",		"Chiamate");
	define("TABLE_COL_THREE_GENERAL",	"Ultima data");
	define("TITLE_CHART_GENERAL",		"Numero chiamate");
	define("TITLE_PIE_CHART_GENERAL",	"Utilizzo per categorie");


	define("TITLE_ERROR",				"Statistiche errori");
	define("DESCRIPTION_TABLE_ERROR",	"Lista dei diversi errori raggruppati per tipologia");
	define("TABLE_COL_ONE_ERROR",		"Tipologia");
	define("TABLE_COL_TWO_ERROR",		"Numero");
	define("TABLE_COL_THREE_ERROR",		"Ultima data");
	define("TITLE_CHART_ERROR",			"");
	define("TITLE_PIE_CHART_ERROR",		"");

?>