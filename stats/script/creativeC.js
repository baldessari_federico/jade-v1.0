$(document).ready(function(){

	// funzione dropdown su liste
	$("li.dropdown span").click(function(event){
		// TO DO insert animation
		elem = $($(this)[0].parentElement)
		elem.toggleClass("dropdown-active")

		child = $($(this)[0].children)
		if(child.text() == "arrow_drop_down")
			child.text("arrow_drop_up")
		else if(child.text() == "arrow_drop_up")
			child.text("arrow_drop_down")
	});

	$("ul.tag-selectable li").click(function(){
		elem 		= $($(this)[0].parentElement)
		classe 		= elem.attr( "data-attr" )
		call 		= elem.attr( "data-call" )
		call_show 	= $(this).attr( "data-link" )

		console.log(elem[0].children)
		all_li = elem[0].children;
		for(i=0; i<all_li.length; i++){
			$(all_li[i]).removeClass( classe )
		}

		$(this).addClass(classe);
		$("."+call).each(function(i, elem){
			$(elem).hide();
		});
		$("."+call_show).show();

	});

	$(".open-nav-side").click(function(){
		$("#velina").removeClass("hide")
		$(".nav-side").addClass("nav-side-open")
	});

	$("#velina").click(function(){
		$("#velina").addClass("hide")
		$(".nav-side").removeClass("nav-side-open")
	});

	$(".changer").change(function(){
		var modifica = $(this).attr("data-attr")
		var valore   = $(this).val()
		if(valore && valore!=""){
			$($($(".code-preview")[0])[0].childNodes).attr("data-"+modifica, valore)
		}
		else{
			$($($(".code-preview")[0])[0].childNodes).removeAttr("data-"+modifica)
		}
		aggiornaCodice()
	});


	reopenNavMenu();

	//MODAL
	//open modal
	$(".modal-trigger").click(function(){
		modalID = $(this).attr("data-modal")
		f = $(this).attr("data-function")
		$("#"+modalID).show()
		if( f ) window[f]()
	});
	
	//close modal
	$(".modal .modal-close").click(function(){
		console.log($(this))
		//salgo fino a trovare un ID
		modalID = ""
		$elem 	= $(this)[0]
		while( modalID == "" ){
			$elem = $elem.parentElement
			modalID = $elem.id
		}
		$("#"+modalID).hide()
	});

	//sottolinea
	hljs.configure({useBR: true});
	$('.code').each(function(i, block) {
	    hljs.highlightBlock(block)
	});

});

function openOnScale( $elem, searchClass, addClass, lastClass ){
	
	$parent = $elem.parentNode
	while( $parent && !( $parent.className == lastClass ) ){

		if( $parent.className == searchClass )
			$parent.className = $parent.className + " " + addClass

		//continuo a salire
		$parent = $parent.parentNode
	}
	return true;
}

function reopenNavMenu(){
	var location_href 	= window.location.href
	var page 			= location_href.match(/(?!.*\/)(.*)/)
	if( page.length ){
		var search = page[0];
		var list = $('.nav-ul [href$="'+search+'"]')
		if( list.length )
			openOnScale( list[0], "dropdown", "dropdown-active", "nav-ul" );
	}
}

function copyInModal(){
	testo 		= $("#prime-code").text()
	html_code  	= 	'<div class="">'+
					'	<div class="paddTB10 center">'+
					'		Copia il codice (Ctrl+V) e chiudi'+
					'	</div>'+
					'	<div class="paddTB20">'+
					'		<textarea class="padd10" rows="5"></textarea>'+
					'	</div>'+
					'</div>'

	$("#modal-copy .modal-body").html(html_code)
	$("#modal-copy textarea"   ).val( testo )
								.select()
}

function toast( stringa, timeout ){
	$toast = $("#toast");
	$toast.text(stringa).show();
	window.setTimeout(function(){
		$toast.css("bottom", "0px");
	}, 100);
	window.setTimeout(function(){
		$toast.hide();
	}, timeout)
}

//called from input link
function modifyCodeFromNum( elem, toModify ){
	valore 	= $($(elem)[0].childNodes[1]).val()
	integer = ( valore.match("[0-9]+") ) ? valore.match("[0-9]+")[0] : null
	$($($(".code-preview")[0])[0].childNodes).attr(toModify, integer)

	aggiornaCodice()
}

function aggiornaCodice(){
	//il codice viene resettato
	$($($(".code-preview")[0])[0].childNodes).html("")

	//mi prendo il codice e lo scrivo nella box per lo sviluppatore
	var codice 		= $(".code-preview").html()
	console.log(codice)
	$("#prime-code").text(codice)

	runJADECode();
	//$("#prime-code").each(function(i, block) {
	    hljs.highlightBlock( $("#prime-code")[0] );
	//});
}