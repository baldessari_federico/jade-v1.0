<?php

	require_once( 'libs/glue.php' );
	require_once( 'pages.php' );

	$urls = array(

		// JADE
		'(/)?' 						=> 	'login',
		'/JADE'						=>  '',
		'/JADE/general'				=> 	'general',
		'/JADE/error'				=>  'error'

	);
	
	glue::stick($urls);
?>