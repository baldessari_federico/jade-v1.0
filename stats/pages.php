<?php

require( "classes.php" );
require_once( 'config.php' );

// PAGINE JADE

// introduzione
class show_index {
	function GET() {
		$values   = array(
			'title'        => TITLE_INTRO,
			'subtitle'     => SUBTITLE_INTRO,
			'description'  => DESC_INTRO,
		);

		$page = new Page("show");
		$page -> render( $values );
	}
}

// statistiche generali di utilizzo
class general {
	function GET() {
		require 'queries.php';
		require_once( 'dictionary.php' );

		$dbHandler = new queryHandler("fidal");

		//carico la tabella
        $dbHandler->runQuery($select_top_site, []);
        $table = $dbHandler->getResult();

        //carico il grafico
        $dbHandler->runQuery($select_count_visit_date, []);
        $chart = $dbHandler->getResult();

        //carico il grafico a torta
        $dbHandler->runQuery($select_top_element_visited, []);
        $pie_chart = $dbHandler->getResult();

        $values   = array(
			'table'        	=>  $table,
			'chart'     	=>  json_encode( $chart ),
			'pie_chart'  	=>  json_encode( $pie_chart )
		);

		$values = array_merge($values, $W_GENERAL);

		$page = new Page("show");
		$page -> render( $values );
	}
}

class error {
	function GET() {
		require 'queries.php';
		require_once( 'dictionary.php' );

		$dbHandler = new queryHandler("fidal");

		//carico la tabella
        $dbHandler->runQuery($select_top_error, []);
        $table = $dbHandler->getResult();

        //carico il grafico
        $dbHandler->runQuery($select_count_error_rate, []);
        $chart = $dbHandler->getResult();

        //carico il grafico a torta
        $dbHandler->runQuery($select_top_error_category, []);
        $pie_chart = $dbHandler->getResult();

        $values   = array(
			'table'        	=>  $table,
			'chart'     	=>  json_encode( $chart ),
			'pie_chart'  	=>  json_encode( $pie_chart )
		);

		$values = array_merge($values, $W_ERROR);

		$page = new Page("show");
		$page -> render( $values );
	}
}

?>