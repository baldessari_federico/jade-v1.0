# README #

La repository ha il solo scopo di illustrare il codice utilizzato per la realizzazione del progetto JADE

- api        (api.atletica.me)
- developers (developers.atletica.me/JADE/v1.0)
- stats      (stats.atletica.me/JADE/general)

Le query, i file e le sezioni di codice che si interfacciano con il sito Atletica.me sono state oscurate per motivi di sicurezza.