var tutti_risultati                 = new Array();
var risultati_gara                  = new Array(); //risultati filtrati per gara e specifica (se esiste la specifica)
var risultati_filtrati_anno         = new Array();
var risultati_filtrati_anno_vento   = new Array();
var risultati_gara_preambolo        = new Array();

carica=true;
doesSupportsSvg=true;
var json;
var data;
var chart;
var options;
var max_width;

function supportsSVG() {
  return !!document.createElementNS && !!document.createElementNS('http://www.w3.org/2000/svg', "svg").createSVGRect;
}

$(document).ready(function(){
	get_p();

	max_width = nee();
  $(window).resize(function(){
    new_width = nee();
    if( max_width > (new_width + 20) || max_width < (new_width - 20) ){
      max_width = new_width;
      carica=false;
      drawChart();
    }
  });

  doesSupportsSvg = supportsSVG();
  if(!doesSupportsSvg){
    $('#chart_div').hide();
    $('#contieni').hide();
    $('#alert-svg').show();
    $('#alert-svg').html("<p>Questa non ci voleva! Il tuo browser non supporta Svg e questo non ci permette di disegnare i grafici. Prova ad aggiornare il tuo dispositivo o installare un browser alternativo</p>");
  }
});


function get_p(){
  if(gara && gara!=""){
    //preparaChart();
    list_misure();  /* scrive il nome delle gare */
    popola_tabella();  //questa funzione si occupa di scrivere le gare in una tabella
  }
}

function changeAnnoOption(sel){
  anno = sel.value;
  carica=true;
  filtraRisultatiPerAnno();  //ci pensa lui poi a filtrarli anche per vento e chiamare il popola_statistica e changeChart
}

function changeGaraOption(sel){
  var gara = sel.value;
  changeGara(gara);
}

//inizio
var numero=0;

function changeGara(num){
  resetListaAnno();

  $("#garaSelect").val(num);
  
  numero=num;  //salvo che numero di gara sto visualizzando
  gara=gare_list[num].gara;  //mi prendo la gara

  carica_risultati_gara(); // a questo punto si susseguono: filtraPerAnno filtraAnnoVento popolaStat changeChart
}
//fine

function popola_tabella(){
	$.post("/query/get_risultati_atleta.php", {id: cache_id, gara: gara})
	.done(function(data){

		tutti_risultati=JSON.parse(data);
		if(tutti_risultati.length>0){
			carica_risultati_gara(); // a questo punto si susseguono: filtraPerAnno filtraAnnoVento popolaStat changeChart
		}
		else
		  $('#graficiGuardian').css("opacity", "0");

	});
}

function list_misure(){
	
	input_testo		= '';
	for(var i=0; i<gare_list.length; i++){
		input_testo +=	'<option value="'+ i +'">'+
							gare_list[i].garaNome+
						'</option>';
	}

	$('#garaSelect').html(input_testo);
}

function carica_risultati_gara(){  //essenzialmente passo da tutti_i_risultati a risultati_gara (filtrati per gara selezionata!)
  
	risultati_gara=[];  //azzero

	var j=0;
	for(var i=0; i<tutti_risultati.length; i++){
	if(tutti_risultati[i].gara==gara){  // filtro per gara
		var nmr_vento=parseFloat(tutti_risultati[i].vento);
		if(isNaN(nmr_vento))
			nmr_vento=0;
			tutti_risultati[i].valore=parseFloat(tutti_risultati[i].valore);  //converto in Float il valore
			risultati_gara[j]=tutti_risultati[i];
			risultati_gara[j].nmr_vento=nmr_vento;  //aggiungo un campo vento in Float (mantengo il vento come stringa in .vento)

			j++;
		}
	}

	risultati_gara_preambolo=risultati_gara.slice(0);
	filtraRisultatiPerAnno();
}


var caricoListaAnno=true;
function filtraRisultatiPerAnno(){

	risultati_filtrati_anno       	= [];
	risultati_filtrati_anno_vento 	= [];
	var num 		= 0;
	var numVento 	= 0;
	var annoAttuale = "mbabane";
	for(var i=0; i<risultati_gara.length; i++){
		if(risultati_gara[i]!=undefined){
			if(caricoListaAnno){
				if(annoAttuale!=risultati_gara[i].anno){
					annoAttuale = risultati_gara[i].anno;
					scriviAnno(annoAttuale);
				}
			}
			if(parseInt(risultati_gara[i].anno)==anno || anno==0){
				risultati_filtrati_anno[num]=risultati_gara[i];
				num++;
				if(risultati_gara[i].nmr_vento<=2.0){
					if(!isNaN(risultati_gara[i].valore) && risultati_gara[i].valore!=null){
						risultati_filtrati_anno_vento[numVento]=risultati_gara[i];
						numVento++;
					}
				}
			}
			else if(parseInt(risultati_gara[i].anno)>anno && anno!=0){
				i=risultati_gara.length+1;
			}
		}
	}

	if(caricoListaAnno){
		opt = '<option value="0" selected class="">Tutti gli anni</option>';
		$("#listaAnno").prepend(opt);
	}
	caricoListaAnno = false;

	if(risultati_gara[0] && risultati_gara[0].comeCresce=="1")
		tabella(2);
	else
		tabella(3);



	// vado in queste funzioni perchè i dati sono cambiati!!
	popola_statistica();
	changeChart();
}

var tmpl_li_anno = '<option value="ANNO" class="">ANNO</option>';
function scriviAnno(anno){
	var anno_li = tmpl_li_anno.replace(/ANNO/g, anno);
	$("#listaAnno").prepend(anno_li);
}

function resetListaAnno(){
	$("#listaAnno").html("");
	anno = 0;
	caricoListaAnno = true;
}



function sortBySpecifica(a,b){
  if(a.specifica==null && b.specifica==null)
    return 0;
  else if(a.specifica==null)
    return 1;
  else if(b.specifica==null)
    return -1;

  else if(parseInt(a.specifica_id) < parseInt(b.specifica_id))
        return 1;
    else if(parseInt(a.specifica_id) > parseInt(b.specifica_id))
        return -1;
    else
        return 0;
}

function sortByDate(a,b)
{
  if(a.data==null && b.data==null)
    return 0;
  else if(a.data==null)
    return 1;
  else if(b.data==null)
    return -1;
  else{
    if(a.data < b.data)
        return 1;
    else if(a.data > b.data)
        return -1;
    else
        return 0;
  }
}

  

function sortByDateInv(a,b)
{
  if(a.data==null && b.data==null)
    return 0;
  else if(a.data==null)
    return 1;
  else if(b.data==null)
    return -1;
  else{
    if(a.data < b.data)
        return -1;
    else if(a.data > b.data)
        return 1;
    else
        return 0;
  }
}

function sortByVal(a,b)
{
  
    //return 1, if you want b to come first
    //return -1, if you want a to come first
    //return 0, if both objects are the same

  if(a.valore==null && b.valore==null)
  return 0;
  else if(a.valore==null)
    return 1;
  else if(b.valore==null)
    return -1;

  if(a.valore < b.valore)
    return 1;
  else if(a.valore > b.valore)
    return -1;
  else{
    if(a.data < b.data)
        return -1;
    else if(a.data > b.data)
        return 1;
    else
        return 0;
  }
}

function sortByValInv(a,b)
{
  if(a.valore==null && b.valore==null)
    return 0;
  else if(a.valore==null)
    return 1;
  else if(b.valore==null)
    return -1;
  else{
    if(a.valore > b.valore)
      return 1;
    else if(a.valore < b.valore)
      return -1;
    else{
      if(a.data < b.data)
          return -1;
      else if(a.data > b.data)
          return 1;
      else
          return 0;
    }
  }
}

function sortByCat(a,b)
{
  if(a.categoria==null && b.categoria==null){
    if(a.data > b.data)
      return -1;
    else if(a.data < b.data)
        return 1;
    else
        return 0;
  }
  else if(a.categoria==null)
    return 1;
  else if(b.categoria==null)
    return -1;

  if(a.categoria > b.categoria)
      return 1;
  else if(a.categoria < b.categoria)
      return -1;
  else{
    if(a.data > b.data)
      return -1;
    else if(a.data < b.data)
        return 1;
    else
        return 0;
  }
}

function sortByLuogo(a,b)
{
  if(a.localita=="" && b.localita=="" || a.localita==null && b.localita==null)
    return 0;
  else if(a.localita=="" || a.localita==null)
    return 1;
  else if(b.localita=="" || b.localita==null)
    return -1;

  if(a.localita > b.localita)
      return 1;
  else if(a.localita < b.localita)
      return -1;
  else{
    if(a.data > b.data)
      return -1;
    else if(a.data < b.data)
        return 1;
    else
        return 0;
  }
}

var template_turno =  '<a title="vedi la gara" href="/manifestazioni/gara/risultati/ORARIO_ID">'+
                      ' <img title="vedi gara" alt="vedi" style="height: 12px;" src="/ico/min-arr.png" />'+
                      '</a>';


function tabella(ordinamento){
  
	var risultati_gara_ord=risultati_gara.slice(0);  //creo un clone ma non è lo stesso array!
	if(ordinamento==1)
		risultati_gara_ord.sort(sortByDate);
	else if(ordinamento==2)
		risultati_gara_ord.sort(sortByVal);
	else if(ordinamento==3)
		risultati_gara_ord.sort(sortByValInv);
	else if(ordinamento==4)
		risultati_gara_ord.sort(sortByLuogo);
	else if(ordinamento==5)
		risultati_gara_ord.sort(sortByCat);

	var testo="";
	for(var i=0; i<risultati_gara_ord.length; i++){
		var g_date=risultati_gara_ord[i].data;
		g_date=goodLookingDateFull(g_date);
		var g_categoria=risultati_gara_ord[i].categoria;
		if(!g_categoria)
			g_categoria="";
		var g_prestazione=risultati_gara_ord[i].prestazione;
		var g_punteggio=risultati_gara_ord[i].punteggio;
		var g_vento=risultati_gara_ord[i].vento;
		if(!g_vento) g_vento="";
		var nmr_vento=risultati_gara_ord[i].nmr_vento;
		var g_localita=risultati_gara_ord[i].localita;
		if(!g_localita) g_localita="";
		var g_orario_id=risultati_gara_ord[i].orario_gara_id;
		if(!g_orario_id) g_orario_id=""; else g_orario_id=template_turno.replace(/ORARIO_ID/g, g_orario_id);

		if(i%2==0)
			testo+='<tr class="';
		else
			testo+='<tr class="azz';
		if(nmr_vento>2.0){
			testo+=' err';
		}
		if(!g_punteggio || g_punteggio==0 || g_punteggio==null)
			g_punteggio="-";

		testo+='">';
		testo+= 	'<td>'+g_date+'</td>'+
					'<td>'+g_categoria+'</td>'+
					'<td>'+g_prestazione+'</td>'+
					'<td>'+g_vento+'</td>'+
					'<td>'+g_punteggio+'</td>'+
					'<td>'+g_localita+'</td>'+
					'<td>'+g_orario_id+'</td>'+
				'</tr>';
	}
	$('#tempi').html(testo);
	$('#table').css("opacity", "1");

}




var tmpl_statistica   =   '<div class="split full-split33 center">'+
                          ' <div class="Cred f25 paddTB5">MAX_VAL</div>'+
                          ' <div class="paddTB5 f16 Cblue">MAX_PUNTI</div>'+
                          ' <div class="f12 op8">migliore</div>'+
                          ' <div class="f12 op8">punti</div>'+
                          '</div>'+
                          '<div class="split full-split33 center">'+
                          ' <div class="Cred f25 paddTB5">AVG_VAL</div>'+
                          ' <div class="paddTB5 f16 Cblue">AVG_PUNTI</div>'+
                          ' <div class="f12 op8">media</div>'+
                          ' <div class="f12 op8">punti</div>'+
                          '</div>'+
                          '<div class="split full-split33 center">'+
                          ' <div class="Cred f25 paddTB5">NUMERO_GARE</div>'+
                          ' <div class="paddTB5 f16 Cblue">SOMMA_PUNTI</div>'+
                          ' <div class="f12 op8">numero gare</div>'+
                          ' <div class="f12 op8">somma punti</div>'+
                          '</div>';

function popola_statistica(){
  
	if(risultati_filtrati_anno_vento.length>0){
		var max=risultati_filtrati_anno_vento[0].valore;
		var maxP=risultati_filtrati_anno_vento[0].punteggio;
		var min=risultati_filtrati_anno_vento[0].valore;
		var minP=risultati_filtrati_anno_vento[0].punteggio;
		var nmr=0;
		var sommaP=0;
		var cresce=risultati_filtrati_anno_vento[0].comeCresce;
		var sommaTempi=0;
		var not_prova_multipla=risultati_filtrati_anno_vento[0].prova_multipla!="0";

		// ci facciamo un giro gratis che si potrebbe evitare
		for(var i=0; i<risultati_filtrati_anno_vento.length; i++){
			if(risultati_filtrati_anno_vento[i]!=undefined){
				if(risultati_filtrati_anno_vento[i].valore<min){
					min=risultati_filtrati_anno_vento[i].valore;
					minP=risultati_filtrati_anno_vento[i].punteggio;
				}
				if(risultati_filtrati_anno_vento[i].valore>max){
					max=risultati_filtrati_anno_vento[i].valore;
					maxP=risultati_filtrati_anno_vento[i].punteggio;
				}
				nmr++;
				sommaP+=parseInt(risultati_filtrati_anno_vento[i].punteggio);
				sommaTempi+=parseFloat(risultati_filtrati_anno_vento[i].valore);
			}
		}

		var avgP=sommaP/nmr;
		var avgVal=sommaTempi/nmr;

		avgVal=Number(avgVal).toFixed(2);
		avgP=Number(avgP).toFixed(0);  //qui si può discutere sul fatto che la media punti non abbia decimali ma credo sia più corretto!

		if(maxP==0 || maxP==null || isNaN(maxP))
			maxP='-';
		if(minP==0 || minP==null || isNaN(minP))
			minP='-';
		if(avgP==0 || avgP==null || isNaN(avgP))
			avgP='-';
		if(sommaP==0 || sommaP==null || isNaN(sommaP))
			sommaP='-';

		if(cresce!=0){
			var best=max;
			var avg=avgVal;
			// me ne frego di tutto. sono misure, non tempi li sparo fuori

			if(not_prova_multipla){
				best=convertToGoodMisure(best);
				avg=convertToGoodMisure(avg);
			}

			statistica=tmpl_statistica.replace(/MAX_VAL/g, best).replace(/MAX_PUNTI/g, maxP).replace(/AVG_VAL/g, avg).replace(/AVG_PUNTI/g, avgP).replace(/NUMERO_GARE/g, nmr).replace(/SOMMA_PUNTI/g, sommaP);
		}
		else{
			// devo convertire i valori in tempi
			var best=convertToTime(min);
			var avg=convertToTime(avgVal);

			statistica=tmpl_statistica.replace(/MAX_VAL/g, best).replace(/MAX_PUNTI/g, minP).replace(/AVG_VAL/g, avg).replace(/AVG_PUNTI/g, avgP).replace(/NUMERO_GARE/g, nmr).replace(/SOMMA_PUNTI/g, sommaP);
		}
	}
	else{
		statistica="nessuna gara";
	}


	$('#MiniStatsBox').html(statistica);
	$('#MiniStatsBox').css("opacity", "1");
}


function convertToGoodMisure(misura){
	var misura=parseFloat(misura);
	if(misura && !isNaN(misura))
		misura=misura.toFixed(2);
	return misura;
}




/* DA SPOSTARE IN UNA CLASSE GENERALE!!! */
var mesi=new Array("Gennaio","Febbraio", "Marzo","Aprile","Maggio","Giugno","Luglio","Agosto","Settembre","Ottobre","Novembre","Dicembre");
var mesi_abbreviati=new Array("gen","feb", "mar","apr","mag","giu","lug","ago","set","ott","nov","dic");

function convertInDayMonth(data){
  if(data && data!=null && data!="0000-00-00"){
    var giorno=data.split("-");
    if(giorno.length==3){
      var mini_anno=giorno[0];
      var mini_mese=giorno[1];
      var mini_giorno=giorno[2];

      if(mini_mese){
        mini_mese=mesi[mini_mese-1];
      }

      var mese=mini_giorno+" "+mini_mese;
      return mese;
    }
    else
      return "";
  }
  else
    return "";
}

function goodLookingDateFullBasic(data){
  if(data && data!=null && data!="0000-00-00"){
    var giorno=data.split("-");
    var mini_anno=giorno[0];
    var mini_mese=giorno[1];
    var mini_giorno=giorno[2];
    if(data.indexOf("-00-00")>0) return mini_anno
    var full_data=mini_giorno+"-"+mini_mese+"-"+mini_anno;
    return full_data;
  }
  else
    return "";
}


function goodLookingDateFull(data){
  if(data && data!=null && data!="0000-00-00"){
    var giorno=data.split("-");
    var mini_anno=giorno[0];
    var mini_mese=giorno[1];
    var mini_giorno=giorno[2];
    if(data.indexOf("-00-00")>0) return mini_anno
    if(mini_mese){
      mini_mese=mesi_abbreviati[mini_mese-1];
    }
    var full_data=mini_giorno+"-"+mini_mese+"-"+mini_anno;
    return full_data;
  }
  else
    return "";
}

function goodLookingDateTime(data){
  var dividi=data.split(" ");

  var mese=convertInDayMonth(dividi[0]);

  var ora=dividi[1].split(":");
  var h=ora[0];
  var min=ora[1];
  
  var data_new=mese+" "+h+":"+min;

  return data_new;
}

function convertToTime(secT){
  var sec_num = parseInt(secT, 10);
  var decimi  = secT*100-sec_num*100;
  var decimi  = Math.round(decimi);
  var hours   = Math.floor(sec_num / 3600);
  var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
  var seconds = sec_num - (hours * 3600) - (minutes * 60);

  var time="";

  if(decimi < 10){
    decimi = "0"+decimi;
  }
  if(minutes==0 && hours==0){
    time=seconds+"."+decimi;
  }
  else{
    if(seconds < 10) {seconds = "0"+seconds;}
    if(hours==0){
      time=minutes+':'+seconds+"."+decimi;
    }
    else{
      if(minutes < 10) {minutes = "0"+minutes;}
      time    = hours+'h'+minutes+':'+seconds+"."+decimi;
    }
  }
  return time;
}



/* graph */
//inizio
function nee(){  //RESIZE GRAFICO 1
  if(document.getElementById("stats-box")){
    width=document.getElementById("stats-box").offsetWidth;
    if(width<400)
      return window.width;
    else
      return (width);
  }
  else{
    return false;
  }
}
//fine

var comeCresceGrafico=0;
function changeChart(){
  $("#chart_div svg").css("opacity", ".4");
  if(!chart)
    preparaChart();

  data  = new google.visualization.DataTable();

  data.addColumn('string', 'years');
  data.addColumn('number', 'gara');
  data.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});

  data.removeRows(0, data.getNumberOfRows())

  if(risultati_filtrati_anno_vento.length)
    comeCresceGrafico=risultati_filtrati_anno_vento[0].comeCresce;
  for(var i=0; i<risultati_filtrati_anno_vento.length; i++){
    var c_date=goodLookingDateFullBasic(risultati_filtrati_anno_vento[i].data);
    var c_date_string = goodLookingDateFull(risultati_filtrati_anno_vento[i].data).replace(/-/g, " ");
    //var c_punteggio=parseInt(tutti_risultati[i].punteggio);
    var c_prestazione=risultati_filtrati_anno_vento[i].prestazione;
    var c_valore=parseFloat(risultati_filtrati_anno_vento[i].valore);
    var c_vento = (risultati_filtrati_anno_vento[i].nmr_vento!=null && risultati_filtrati_anno_vento[i].vento!=null) ? "vento: "+risultati_filtrati_anno_vento[i].nmr_vento+"m/s" : "";
    data.addRow([c_date, c_valore, createCustomHTMLContent(c_date_string, c_prestazione, c_vento)]);
  }

  // Create our data table out of JSON data loaded from server.
  setTimeout(drawChart, 1);

  /* effetto opacity per caricamento */
}

function createCustomHTMLContent(data, prestazione, vento){
  var tooltip = '<div class="tooltipMe padd10">'+
                ' <div class="paddTB5">'+data+'</div>'+
                ' <div class="space5"></div>'+
                ' <div class="text op7">Prestazione:</div>'+
                ' <div class="primary Cblue f25">'+prestazione+'</div>'+
                ' <div class="other">'+vento+'</div>'+
                '</div>';

  return tooltip;
}

var primo=true;

/*
    animation:{
        duration: 30,
        easing: 'out'
    },
*/

function preparaChart(){
  options = {
    title: '',
    width: max_width,
    series: {0:{color:'#0066CC', lineWidth:1}},
    backgroundColor: {fill:'transparent'},
    curveType: 'function',
    pointSize: 3,
    width: 0,
    tooltip: {isHtml: true},
    legend: {position: 'none'},
    hAxis: {textStyle:{color: 'gray', fontSize: '12'}, gridlines:null, slantedText: false},
    vAxis: {baselineColor: 'none', textStyle:{color: 'gray', fontSize: '12'}, gridlines: {color: 'transparent'}},
      //tooltip: {isHtml: true}
  };

  chart = new google.visualization.LineChart(document.getElementById('chart_div'));
  data  = new google.visualization.DataTable();
  data.addColumn('string', 'years');
  data.addColumn('number', 'gara');
  data.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});

  //drawChart();
}

function drawChart(){
  if(doesSupportsSvg){
    //google.visualization.events.addListener(chart, 'select', selectHandler);
    chart.draw(data, options);
    change_y_values("chart_div");
    $("#chart_div svg").css("opacity", "1");
  }
  // else{
  //   ;
  // }
}


//CONTROLLARE SE DUPLICATA
function change_y_values(div_container){
  if(comeCresceGrafico=='0'){
    var arr_elem_y=$('#'+div_container+' svg text[text-anchor="end"]');
    for(var i=0; i<arr_elem_y.length; i++){
      writeGoodYValue($(arr_elem_y[i]));
    }
  }
}

function writeGoodYValue($elem){
  var stringNumToConvert = $elem[0].textContent;

  var nmr_da_sostituire  = convertFormatToFloat(stringNumToConvert);
  if(!isNaN(nmr_da_sostituire)){
    nmr_da_sostituire=convertToTime(nmr_da_sostituire)
    $elem.text(nmr_da_sostituire);
  }
}

function convertFormatToFloat(stringNum){
  var indexPunto   = stringNum.indexOf(".");
  var indexVirgola = stringNum.indexOf(",");
  if(indexPunto>0 && indexVirgola>0){
    if(indexPunto>indexVirgola){
      return parseFloat(stringNum.replace(/,/g,""));
    }
    else{
      return parseFloat(stringNum.replace(/\./g,"").replace(/,/g,"."));
    }
  }
  else{
    return parseFloat(stringNum.replace(/\./g,"").replace(/,/g,"."));
  }
}