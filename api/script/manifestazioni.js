// FUNZIONI MANIFESTAZIONI

var data_today  = new Date();
var mini_anno   = data_today.getFullYear();
var mini_mese   = data_today.getMonth();
var mini_giorno = data_today.getDate();

var mese="00";

var tmpl_manifestazione =   '<a target="_blank" href="http://atletica.me/manifestazioni/gara/M_ID" >'+
                            '   <div class="paddTB10 M_CLASS">'+
                            '     <div class="no-font">'+
                            '       <span class="split full-split15 f16">'+
                            '         M_DATA'+
                            '       </span>'+
                            '       <span class="split full-split70">'+
                            '         <p class="f16">M_NOME</p>'+
                            '         <p class="f13 op7 no-old">M_TIPOLOGIA</p>'+
                            '       </span>'+
                            '       <span class="split full-split15">'+
                            '          <p class="f16 Cblue">'+
                            '             M_REG_CODE'+
                            '          </p>'+
                            '          <p class="f12 no-old">'+
                            '             M_REG_CITY'+
                            '          </p>'+
                            '       </span>'+
                            '     </div>'+
                            '   </div>'+
                            '</a>';

var mesi = new Array("gennaio","febbraio", "marzo","aprile","maggio","giugno","luglio","agosto","settembre","ottobre","novembre","dicembre");

var regioni=new Array(" ", "Abruzzo","Basilicata","Calabria","Campania","Emilia-Romagna","Friuli-Venezia Giulia","Lazio","Liguria","Lombardia","Marche","Molise","Piemonte", "Puglia", "Sardegna","Sicilia","Toscana","Trentino-Alto Adige","Umbria","Valle d'Aosta","Veneto");


$( document ).ready( function(){
  initializeSearch();
});

function initializeSearch(){
  if(!regione){
    regione = regioni[1];
  }
  $("#select_regione").val(regione);

  getManifestazioni();
}

function changeMese(){

  mese=$("#select_mese").val();
  getManifestazioni();

}

function changeRegione(){

  regione=$("#select_regione").val();
  getManifestazioni();

}

function getManifestazioni(){

  isDaOggi = false;
  if(mese=="00"){
    //significa che è l'opzione di default (non è selezionato un mese: da oggi in poi)
    isDaOggi = true;
  }

  if( regioni.indexOf( regione ) >= 0 ){

    $.post( "/query/get_manifestazioni.php", { regione: regione, mese: mese } )
    .done( function( data ){
      data=JSON.parse( data );
      scriviManifestazione( data, isDaOggi );
    });

  }
  else{
    //error!
  }
}

function scriviManifestazione( data, isDaOggi ){

  var testo="";

  if(data.length<1){
    testo='<div class="bigLine mese">- Non sono previste gare -</div>';
  }
  else{

    var m_mese_current = "false_mese";

    for(var i=0; i<data.length; i++){

      var m_mese        = data[i].mese-1;
      var m_id          = data[i].id;
      var m_inizio      = data[i].inizio;
      var m_fine        = data[i].fine;
      var m_nome        = data[i].nome;
      var m_regione     = data[i].regione;
      var m_provincia   = data[i].provincia;
      var m_localita    = data[i].localita;
      var m_country     = (data[i].country_code)  ? data[i].country_code.toLowerCase() : null;
      var abbr_livello  = data[i].abbr_livello;
      var citta         = data[i].luogo;
      var m_tipologia   = (data[i].tipologia)     ? data[i].tipologia : '';

      if( m_mese_current != m_mese ){
        m_mese_current  =  m_mese
        testo           += '<div class="semi-title paddTB10">'+mesi[ m_mese ]+'</div>';
      }

      var m_classe  = (isDaOggi && mini_giorno > m_fine && mini_mese >= m_mese_current) ? "op5 old-man" : "";
      var m_data    = ( m_inizio!=m_fine ) ? m_inizio+"/"+m_fine : m_inizio;

      var errore  = false;
      if(abbr_livello=='I')
          m_reg_code = ( m_country ) ? '<img alt="flag" src="http://atletica.me/ico/flag/24/'+m_country+'.png">' : m_provincia;
      else{
        if(m_country!='it'){  // gestisco l'errore. vedi sopra
          m_reg_code = '<img alt="flag" src="http://atletica.me/ico/flag/24/it.png">';
          errore=true;
        }
        else
          m_reg_code = m_provincia;
      }
      m_reg_city = ( errore ) ? citta : m_localita;

      testo += tmpl_manifestazione.replace(/M_CLASS/g, m_classe)
                                  .replace(/M_ID/g, m_id)
                                  .replace(/M_DATA/g, m_data)
                                  .replace(/M_NOME/g, m_nome)
                                  .replace(/M_TIPOLOGIA/g, m_tipologia)
                                  .replace(/M_REG_CODE/g, m_reg_code)
                                  .replace(/M_REG_CITY/g, m_reg_city)

    }//chiudo ciclo for

  }//chiudo else

  $('#tableBox').html(testo);

}//chiudo funzione