/*                                                    *\
--------------------------------------------------------

			JADE 1.0 - Creative Composer				

--------------------------------------------------------
- no jquery needed
- library used: davidjbradshaw -> iframe-resizer
- css included: insider-jade.css

VERSION     : 1.0
document    : developers.atletica.me/JADE/v1.0/
last-update : 11/08/2015

Original host -> api.atletica.me/jade.js

\*                                                    */



var BASE_URL 	= "http://api.atletica.me/JADE/v1.0/"

// vanilla.js
document.addEventListener("DOMContentLoaded", function(event) { 

	var head   = document.getElementsByTagName('head')[0]

	//load insiderJadeCss
	var cssId = 'insiderJadeCss'
	if ( !document.getElementById(cssId) ) {
	    var link   = document.createElement('link')
	    link.id    = cssId
	    link.rel   = 'stylesheet'
	    link.type  = 'text/css'
	    link.href  = 'http://api.atletica.me/css/insider-jade.css'
	    link.media = 'all'

	    head.appendChild(link)
	}

	//load library for resize iframe
	var jsID = 'iframeResizer'
	if ( !document.getElementById(jsID) ) {
	    var jqTag    = document.createElement('script')
	    jqTag.id     = jsID
	    jqTag.type   = 'text/javascript'
	    jqTag.src    = 'http://davidjbradshaw.com/iframe-resizer/js/iframeResizer.min.js'
	    jqTag.onload = runJADECode

	    head.appendChild(jqTag)
	}

});

// CLASS

// JadeElem
// converts information in div to iframe
function JadeElem ( $elem, name ) {

    this.elem = $elem
    this.name = name
    this.id
    this.options = []

    this.getDataAttributes = function () {
    	var el   = this.elem
	    var data = [];

	    [].forEach.call( el.attributes, function( attr ) {

	        if ( /^data-/.test( attr.name ) ) {

	            var camelCaseName = attr.name
	            						.substr(5)
	            						.replace(
	            							/-(.)/g,
	            							function ($0, $1) {
								                return $1.toUpperCase()
								            }
								        )

	            obj = {	
	            		"name": camelCaseName,
	            		"value": attr.value
	            	  }
	            if( camelCaseName!="attr" )
	            	data.push(obj)
	        }
	    });
	    return data
	}

    this.retriveInfo = function() {
    	this.id 	 = parseInt( this.elem.getAttribute("data-attr") )
    	this.options = this.getDataAttributes()
    }

    this.createFrame = function() {
		var url = BASE_URL + this.name + "/"

		if ( this.id && !isNaN( this.id ) )
			url = 	url.replace( "<ID>", this.id )

		//inserisco url di origine
		url += "?origin_url=" + window.location.href

		//inserisco parametri opzionali
		for ( var i = 0; i < this.options.length; i++ )
			url += "&" +this.options[i]["name"] + "=" + this.options[i]["value"]

		//creo l'iframe
		frame = document.createElement('iframe')
		frame.setAttribute('src', 	url)
		frame.setAttribute('width', '100%')

		//lo collego all'elemento
		this.elem.appendChild(frame)
    }

}

//function
function handleFrame($elem, nome, isLink){
	for (var i = 0; i < $elem.length; i++) {
		var jElem = new JadeElem( $elem[ i ], nome )
		jElem.retriveInfo()
		jElem.createFrame()
	}
}

function firstParentType( $elem, search ){
	$parent = $elem.parentNode
	while( $parent && !( $parent.nodeName.toLowerCase() === search.toLowerCase() ) ){
		//continuo a salire
		$parent = $parent.parentNode
		console.log("fu");
	}
	console.log("ok");
	return $parent
}

function calculateClassBestPosition( $link ){

	//aggiungo position:relative al padre
	$parent = firstParentType( $link, "div" )
	position = $parent.style.position
	if( position == null || position == "" || position == "static" ){
		$parent.style.position = "relative"
	}

	ampiezza 	= $link.offsetWidth
	offsetLeft 	= $link.offsetLeft
	offsetRight = $parent.offsetWidth - ampiezza - offsetLeft

	if( offsetLeft + ampiezza > 350 ){
		return ""
	}
	else if( offsetRight + ampiezza > 350 ){
		return " j_iframe_right"
	}
	else{
		return " j_iframe_center"
	}
}

function modifyLinks(){

	//modifico il collegamento ad atleta
	$all_link = document.querySelectorAll('*[href^="http://atletica.me/atleta/"]')
	var rExp = /\d+/;
	[].forEach.call( $all_link, function( $link ){
		hrefLink = $link.getAttribute("href")
		attr_id  = rExp .exec(hrefLink)
		if( attr_id ){
			$link.setAttribute("data-attr", attr_id)

			//calcolo dove posizionare l'iframe
			other_class = calculateClassBestPosition( $link )

			$link.className = $link.className+other_class+" j-badge-atleta-pb"
		}
	});
}

// JADE code
function runJADECode(){

	modifyLinks();

	$all_element  = []

	$all_element
	.push(
		{
			'arr' 	: document.getElementsByClassName("j-badge-atleta"),
			'nome' 	: "athlete/<ID>/general_info"
		},
		{
			'arr' 	: document.getElementsByClassName("j-badge-atleta-pb"),
			'nome' 	: "athlete/<ID>/extended_info"
		},
		{
			'arr' 	: document.getElementsByClassName("j-atleta-pb"),
			'nome' 	: "athlete/<ID>/pb"
		},
		{
			'arr' 	: document.getElementsByClassName("j-atleta-carriera"),
			'nome' 	: "athlete/<ID>/results"
		},
		{
			'arr' 	: document.getElementsByClassName("j-societa-badge"),
			'nome' 	: "club/<ID>/general_info"
		},
		{
			'arr' 	: document.getElementsByClassName("j-societa-lista-atleti"),
			'nome' 	: "club/<ID>/list_athletes"
		},
		{
			'arr' 	: document.getElementsByClassName("j-societa-records-anno-curr"),
			'nome' 	: "club/<ID>/records/curr_year"
		},
		{
			'arr' 	: document.getElementsByClassName("j-gare-ricerca"),
			'nome' 	: "meetings/search"
		},
		{
			'arr' 	: document.getElementsByClassName("j-risultati-tempo-reale"),
			'nome' 	: "news/results/real_time"
		}
	);

	for(i=0; i<$all_element.length; i++){

		$arr_elem 	= $all_element[i]["arr"]
		arr_nome 	= $all_element[i]["nome"]

		if ($arr_elem.length>0) {
			handleFrame($arr_elem, arr_nome)
		}
	}

	// tutti gli elementi sono stati serviti
	//$badge_atleta.addClass("jade");

	iFrameResize()
}