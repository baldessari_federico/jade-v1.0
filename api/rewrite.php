<?php

	require_once( 'libs/glue.php' );
	require_once( 'pages.php' );

	$j1 = '/JADE/v1.0/';

	$urls = array(

		// JADE v1.0
		$j1.'athlete/(\d+)/general_info/(.*)' 		=> 	'badge_atleta',
		$j1.'athlete/(\d+)/extended_info/(.*)' 	=> 	'badge_atleta_esteso',
		$j1.'athlete/(\d+)/pb/(.*)' 					=> 'atleta_pb',
		$j1.'athlete/(\d+)/results/(.*)' 				=> 'atleta_carriera',

		$j1.'club/(\d+)/general_info/(.*)' 				=> 'badge_societa',
		$j1.'club/(\d+)/list_athletes/(.*)' 		=> 'societa_lista_atleti',
		$j1.'club/(\d+)/records/curr_year/(.*)' 				=> 'societa_record_anno_attuale',

		$j1.'meetings/search/?(.*)' 				=> 'ricerca_manifestazioni',

		$j1.'news/results/real_time/(.*)' 				=> '',
	);

	//(\?\w+=\w+)?(&(\w+=\w+))
	
	glue::stick($urls);
?>