<?php

require_once( 'config.php'  );
require(      'classes.php' );

// PAGINE JADE

// introduzione
class badge_atleta {
	
	function GET( $matches ){
		require_once( 'dictionary.php' );
		if( $matches && count($matches)>1 ){

			$id 	= (int)$matches[1];
			$atleta = new Atleta( $id );

			if( $atleta->getGeneralInfo() ){
				$values   = array(
					'Atleta'        => $atleta,
					'classe'		=> "badge"
				);
				if( !empty($_GET["width"]) ) $values["more_classe"] = $_GET["width"];

				$values 	= array_merge( $values, $W_ATHLETE );
				$page 		= new Page("badge_atleta", $id);
				$page 		-> render( $values );
			}
			else{
				$e = new Error(3, "impossibile ottenere informazioni", "atleta: ".$id);
				$e->stampError();
			}
		}
		else{
			$e = new Error(4, "parametri mancanti", "");
			$e->stampError();
		}
	}
}

class badge_atleta_esteso {

	function GET( $matches ) {
		require_once( 'dictionary.php' );
		if( $matches && count($matches)>1 ){

			$id 		= (int)$matches[1];
			$atleta = new Atleta( $id );

			if( $atleta->getGeneralInfo() ){
				$atleta -> getPersonalBest();
				$values   = array(
					'Atleta'        => $atleta,
					'classe'		=> "badge",
					'caricaPersonali' => true
				);

				$values 	= array_merge( $values, $W_ATHLETE );
				$page 		= new Page("badge_atleta", $id);
				$page 		-> render( $values );
			}
			else{
				$e = new Error(3, "impossibile ottenere informazioni", "atleta: ".$id);
				$e->stampError();
			}
		}
		else{
			$e = new Error(4, "parametri mancanti", "");
			$e->stampError();
		}
	}

}

class atleta_pb {
	function GET( $matches ) {
		require_once( 'dictionary.php' );
		if( $matches && count($matches)>1 ){

			$id 		= (int)$matches[1];
			$atleta = new Atleta( $id );

			if( $atleta->getGeneralInfo() ){
				$atleta -> getPersonalBest();
				$values   = array(
					'Atleta'        => $atleta,
					'classe'		=> "badge",
				);

				$values 	= array_merge( $values, $W_ATHLETE );
				$page 		= new Page("atleta_pb", $id);
				$page 		-> render( $values );
			}
			else{
				$e = new Error(3, "impossibile ottenere informazioni", "atleta: ".$id);
				$e->stampError();
			}
		}
		else{
			$e = new Error(4, "parametri mancanti", "");
			$e->stampError();
		}
	}
}

class atleta_carriera {
	function GET( $matches ) {
		require_once( 'dictionary.php' );
		if( $matches && count($matches)>1 ){

			$id 		= (int)$matches[1];
			$atleta = new Atleta( $id );

			if( $atleta->getGeneralInfo() ){
				$atleta -> getPersonalBest();
				$atleta -> getListGare();
				$values   = array(
					'Atleta'        => $atleta,
					'classe'		=> "atleta-carriera",
				);

				$values 	= array_merge( $values, $W_ATHLETE );
				$page 		= new Page("atleta_carriera", $id);
				$page 		-> render( $values );
			}
			else{
				$e = new Error(3, "impossibile ottenere informazioni", "atleta: ".$id);
				$e->stampError();
			}
		}
		else{
			$e = new Error(4, "parametri mancanti", "");
			$e->stampError();
		}
	}
}




//SOCIETA'

class badge_societa {
	function GET( $matches ) {
		require_once( 'dictionary.php' );
		if( $matches && count($matches)>1 ){

			$id 		= (int)$matches[1];
			$societa 	= new Club( $id );

			if( $societa->getGeneralInfo() ){
				$values   = array(
					'Societa'       => $societa,
					'classe'		=> "badge",
				);

				$values 	= array_merge( $values, $W_CLUB );
				$page 		= new Page("badge_societa", $id);
				$page 		-> render( $values );
			}
			else{
				$e = new Error(3, "impossibile ottenere informazioni", "societa: ".$id);
				$e->stampError();
			}
		}
		else{
			$e = new Error(4, "parametri mancanti", "");
			$e->stampError();
		}
	}
}

class societa_lista_atleti {
	function GET( $matches ) {
		require_once( 'dictionary.php' );
		if( $matches && count($matches)>1 ){

			$id 		= (int)$matches[1];
			$societa 	= new Club( $id );

			if( $societa->getGeneralInfo() ){


				$divider = ( !empty($_GET["divider"]) ) ? $_GET["divider"] : 'specialita';
				if( $divider!='specialita' && $divider!='categoria' && $divider!='alfabeto' ){
					$e = new Error(5, "parameter error");
					$e->stampError();
				}

				$societa->getAllAthletes();
				$societa->sortListAthletes( $divider );
				$values   = array(
					'Societa'       	=> $societa,
					'classe'			=> "",
					'badge_class'		=> "badge",
					'attribute'			=> $divider
				);

				$values 	= array_merge( $values, $W_CLUB );
				$page 		= new Page("societa_lista_atleti", $id);
				$page 		-> render( $values );
			}
			else{
				$e = new Error(3, "impossibile ottenere informazioni", "societa: ".$id);
				$e->stampError();
			}
		}
		else{
			$e = new Error(4, "parametri mancanti", "");
			$e->stampError();
		}
	}
}

class societa_record_anno_attuale {
	function GET( $matches ){
		require_once( 'dictionary.php' );

		if( $matches && count($matches)>1 ){

			$id 		= (int)$matches[1];
			$societa 	= new Club( $id );
			
			$values   = array(
				'Societa'       	=> $societa,
				'classe'			=> "",
				'titolo'			=> "RECORD ANNO ATTUALE"
			);

			$values 	= array_merge( $values, $W_CLUB );
			$page 		= new Page("societa_record_anno_attuale", $id);
			$page 		-> render( $values );
		}
		else{
			$e = new Error(4, "parametri mancanti", "");
			$e->stampError();
		}
	}
}

class ricerca_manifestazioni {
	function GET() {
		require_once( 'dictionary.php' );

		$regione = ( !empty($_GET["default"]) ) ? $_GET["default"] : 'Abruzzo';

		$val = new Validator();
		if( $val->isRegione($regione) ){

			$values   = array(
				'classe'			=> "",
				'regione'			=> $regione
			);

			$values = array_merge($values, $W_MEETINGS);
			$values = array_merge($values, $W_ITALIAN_LAND);
			$values = array_merge($values, $W_MONTH);

			$page 		= new Page("ricerca_manifestazioni", null);
			$page 		-> render( $values );
		}
		else{
			$e = new Error(4, "parametri mancanti", "");
			$e->stampError();
		}
	}
}

?>