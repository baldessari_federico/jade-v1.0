<?php	

#GESTORE DELLE PAGINE
class Page {
    var $namePage;
    var $specific;

	public function __construct( $name, $specific ){
		
		$this->namePage = $name;
		$this->specific = $specific;

    }

	public function render( $values ){

		try{
			require_once( 'libs/Twig/Autoloader.php' );
			Twig_Autoloader::register();
			$loader   = new Twig_Loader_Filesystem( ROOT_DIR.'/templates' );
			$twig     = new Twig_Environment( $loader );
			$template = $twig->loadTemplate( $this->namePage );

			if( !empty( $_GET["origin_url"] ) )
				$this->registerAction( $_GET["origin_url"] );

			echo $template->render( $values );

		} catch( Exception $error ) {
			//abbiamo catturato un errore in twig
			$e = new Error(0, "error rendering Twig", "Pagina: ".$this->namePage);
			$e->stampError();
		}
	}

	public function registerAction( $origin_url ){
		require( ROOT_DIR."/queries.php" );

		$exploded_url	= explode( "/", $origin_url );
		$protocol 		= $exploded_url[0];
		$base_url  		= $exploded_url[2];

		$array = array(
                    ":base_url" 	=> $base_url,
                    ":protocol" 	=> $protocol,
                    ":origin_url" 	=> $origin_url,
                    ":element" 		=> $this->namePage,
                    ":specific_id" 	=> $this->specific
                 );

		$dbHandler = new queryHandler("fidal");
        $dbHandler->runQuery($q_insert_action, $array);
	}
}

class Error{
	var $error;

	public function __construct( $codice, $message, $specific_message ){
		$this->error = array(
				"code"	  			=> $codice,
				"message" 			=> $message,
				"specific_message" 	=> $specific_message
			);

		$this->insertError();
	}

	private function insertError(){

		//se non è un errore di connessione al db
		if( $this->error["code"] != ERROR_DB_CONNECTION ){

			require( ROOT_DIR."/queries.php" );

			$array = array(
	                    ":code_error" 		=> $this->error["code"],
	                    ":message" 			=> $this->error["message"],
	                    ":specific_message" => $this->error["specific_message"]
	                 );

			$dbHandler = new queryHandler("fidal");
	        $dbHandler->runQuery($q_insert_error, $array);

	    }
	}

	public function stampError(){
		try{
			require_once( 'libs/Twig/Autoloader.php' );
			Twig_Autoloader::register();
			$loader   = new Twig_Loader_Filesystem( ROOT_DIR.'/templates' );
			$twig     = new Twig_Environment( $loader );
			$template = $twig->loadTemplate( "error" );

			echo $template->render( $this->error );

		} catch( Exception $error ) {
			//abbiamo catturato un errore in twig
			;
		}

		exit;
	}
}

class HtmlConverter {
	var $code;

	public function __construct( $code ){
		$this->code = $code;
    }

    public function htmlCode(){
    	$htmlCode = htmlentities($this->code);
		return $htmlCode;
	}

	public function normCode(){
		return $this->code;
	}
}

# Convertitore di date da formato aaaa-mm-gg a gg-mm-aaaa
# Consente di accedere a numerosi formati di stampa date
class DateConverter{
	var $date;
	var $anno   = null;
	var $mese   = null;
	var $giorno = null;

	public function __construct( $date ){
		$this->date = $date;
		$this->italianFormatDate();
    }

    // TODO: gestire formati diversi come 2015/08/09
    private function italianFormatDate(){

    	$date_exploded = explode("-", $this->date);
    	if(count($date_exploded) == 3){
	    	if( $this->isFormatDate( $date_exploded, 2, 2, 4 ) ){
	    		$this->changeFormatDateToItalian( $date_exploded, 2, 1, 0 );
	    		return true;
	    	}

	    	// non è il formato corretto
	    	// controllo se almeno è accettabile
	    	if( $this->isFormatDate( $date_exploded, 4, 2, 2 ) ){
	    		$this->changeFormatDateToItalian( $date_exploded, 0, 1, 2 );
	    		return true;
	    	}

	    }
	    $this->date = null;
	    return false;
    }

    private function isFormatDate($date, $anno, $mese, $giorno){

		if( strlen($date[0]) == $anno &&
	    	strlen($date[1]) == $mese &&
	    	strlen($date[2]) == $giorno)
	    	return true;

	    return false;

    }

    private function changeFormatDateToItalian($date, $p_anno, $p_mese, $p_giorno){
    	$this->date   = $date[$p_giorno]."-".$date[$p_mese]."-".$date[$p_anno];
    	$this->anno   = $date[$p_anno  ];
    	$this->mese   = $date[$p_mese  ];
    	$this->giorno = $date[$p_giorno];
    }

    public function getFullDate(){
    	if( $this->isValidNum($this->anno) && $this->isValidNum($this->mese) && $this->isValidNum($this->giorno) )
    		return $this->date;
    	else if ( $this->isValidNum( $this->anno ) )
    		return $this->anno;
    	else
    		return "";
    }

    public function getDay(){
    	if($this->giorno!=null)
    		return $this->giorno;
    	else
    		return "";
    }

    public function getMonth(){
    	if($this->mese!=null)
	    	return $this->mese;
	    else
	    	return "";
    }

    public function getSmallMonth(){
    	if($this->mese!=null){
    		$mese = (int)$this->mese;
	    	$mesi_abbreviati=array("", "gen","feb", "mar","apr","mag","giu","lug","ago","set","ott","nov","dic");

	    	return $mesi_abbreviati[$mese];
	    }
	    else
	    	return "";
    }

    public function getAge(){
    	$eta=null;
    	if ( $this->isNotNull() ){
    		$oggi         = explode("-", date("Y-m-d"));

    		if ($oggi[1] > $this->mese)
    			$eta = $oggi[0] - $this->anno;
    		elseif ($oggi[1] >= $this->mese && $oggi[2] >= $this->giorno)
    			$eta = $oggi[0] - $this->anno;
    		else
    			$eta = $oggi[0] - $this->anno - 1;
    	}
    	return $eta;
    }

    public function stampAge(){
    	$age = $this->getAge();
    	if($age == null || $age == 0 || $age>220){
    		return "";
    	}
    	return $age;
    }

	public function isNotNull(){
		$date = $this->date;
		if( $date!="00-00-0000" && $date!=null && $date!="" )
			return true;
		else
			return false;
	}

	private function isValidNum( $num ){
		$num = (int)$num;
		if( $num && $num!=null && $num!=0 )
			return true;
		return false;
	}

	//mostro la data così come è. senza gestire null o eccezioni sull'anno
	public function getDateLikeItIs(){
		return $this->date;
	}

	//mostro solo l'anno mentre giorno e mese sono a zero
	public function getYearOtherZeroBadFormat(){
		return $this->anno."-00-00";
	}

}

#GESTORE DELLE QUERY
class queryHandler {
	var $dbHandler;
	var $status;
	var $sth;

	public function __construct( $dbname ){
		try {
			$this->dbHandler = $this->db_connect( $dbname );
		}
		catch ( PDOException $e ) {
			$e = new Error(1, "error connecting to database", "database ".$dbname);
			$e->stampError();
		}
    }

	public function runQuery( $query, $array_parametri ){

		//CODICE OSCURATO PER MOTIVI DI SICUREZZA

	}

	public function getResult(){
		$result = $this->sth->fetchAll();
		return $result;
	}

	public function getFirstRow(){
		return $this->sth->fetch();
	}

	public function noError(){
		return $this->status;
	}

	public function searchError( $query ){
		if( !$this->status ){
			$e = new Error(2, "query error", "query: ".$query);
			$e->stampError();
		}
	}

	public function getResultJSON(){
		$result = $this->sth->fetchAll();
		return json_encode($result);
	}

	public function getNumRows(){
		$rows = $this->sth->fetch( PDO::FETCH_NUM );
		return $rows;
	}

	public function close(){
		$this->dbHandler = null;
	}

	//connessione al database
	public function db_connect( $dbname ){
		
		//CODICE OSCURATO PER MOTIVI DI SICUREZZA
		
	}
}


#PAGINA ATLETA
class Atleta {
	
	var $ID;
	var $genInfo 		= null;
	var $personalBest 	= null;
	var $listGare       = null;

	public function __construct( $id ){
		$this->ID 		= $id;
    }

    public function getID(){
    	return $this->ID;
    }

    public function getGeneralInfo(){
    	// mi occupo di trovare nome, cognome, eta
    	require( ROOT_DIR."/queries.php" );
    	$query = new queryHandler( "django" );
    	$array = array(
					":id" => $this->ID
				);
		$query -> runQuery( $q_select_info_athlete, $array );
		$result = $query -> getResult();

		if($result && count($result)>0){
			$this->genInfo 			= $result[0];
			return true;
		}
		return false;
    }

    public function setGeneralInfo($genInfo){
    	$this->genInfo = $genInfo;
    }

    public function getPersonalBest(){
    	// mi occupo di trovare i PB
    	require( ROOT_DIR."/queries.php" );
    	$query = new queryHandler( "django" );
    	$array = array(
					":id" => $this->ID
				);
		$query -> runQuery( $q_select_pb_athlete, $array );
		$result = $query -> getResult();

		if($result && count($result)>0){
			$this->personalBest = $result;
			return true;
		}
		return false;
    }

    public function getListGare(){
    	// mi occupo di trovare la lista delle gare
    	require( ROOT_DIR."/queries.php" );
    	$query = new queryHandler( "django" );
    	$array = array(
					":id" => $this->ID
				);
		$query -> runQuery( $q_select_list_gare, $array );
		$result = $query -> getResult();

		if($result && count($result)>0){
			$this->listGare = $result;
			return true;
		}
		return false;
    }

    // general information about athlete
    public function getAttribute( $attribute ){
    	if( !empty($this->genInfo[ $attribute ]) )
    		return $this->genInfo[ $attribute ];
    	else
    		return null;
    }

    public function getNomeCognome(){
    	return $this->genInfo["nomecognome"];
    }

    public function getDataNascita(){
    	$data_nascita = new DateConverter( $this->genInfo["data_nascita"] );
    	return $data_nascita;
    }

    public function stampDataNascita(){
    	$data_nascita = $this->getDataNascita();
    	return $data_nascita->getFullDate();
    }

    public function getFaceID(){
    	return $this->genInfo["id_face"];
    }

    public function haveFaceID(){
    	return ( $this->genInfo["id_face"] != null ) ? true : false;
    }

    public function getUrlFotoFacebook(){

    }

    public function getUrlFoto( $dim_photo ){
    	if($this->genInfo["id_face"]!=null){
    		//carico la foto da facebook
    		$dim_photo = $dim_photo+20;
    		return "https://graph.facebook.com/".$this->getFaceID()."/picture?width=".$dim_photo."&height=".$dim_photo;
    	}
    	else{
    		$dim_photo = $dim_photo+20;
    		return "http://atletica.me/img/avatar/profile.php?name=".$this->getNomeCognome().'&dim='.$dim_photo.'&id='.$this->getID();
    	}
    }

    public function stampPhoto( $dim_photo, $class ){
    	if($this->genInfo["id_face"]!=null){
    		//carico la foto da facebook
    		$dim_photo = $dim_photo+20;
    		return '<img alt="" class="'.$class.'" src="https://graph.facebook.com/'.$this->getFaceID().'/picture?width='.$dim_photo.'&height='.$dim_photo.'" >';
    	}
    	else{
    		//genero la foto da php
    		$dim_photo = $dim_photo+20;
    		return '<img alt="" class="'.$class.'" src="http://atletica.me/img/avatar/profile.php?name='.$this->getNomeCognome().'&dim='.$dim_photo.'&id='.$this->getID().'" >';
    		//return "";
    	}
    }

    public function getSEOUrl(){
    	$nomecognome = str_replace(" ", "-", $this->getNomeCognome());
    	return "/atleta/".$nomecognome."/".$this->getID();
    }

    public function getSEOUrlClub(){
    	return "/societa/".$this->genInfo["societa_id"];
    }

    public function getSpecialita(){
    	if($this->genInfo["sesso"] == "F")
    		return $this->genInfo["specialita_femminile"];
    	return $this->genInfo["specialita_maschile"];
    }

    public function haveSpecialita(){
    	if( !empty( $this->genInfo["specialita_maschile"]) )
    		return true;
    	return false;
    }

    public function haveNomeSocieta(){
    	if( !empty( $this->genInfo["denominazione"]) )
    		return true;
    	return false;
    }

    public function getNomeSocieta(){
    	return $this->genInfo["denominazione"];
    }

    public function getGender(){
    	if($this->genInfo["sesso"])
    		return $this->genInfo["sesso"];
    	return "";
    }

    public function isTesserato(){
    	// avoid tesserato = null
    	if(	$this->genInfo["tesserato"] &&
    		$this->genInfo["tesserato"] == 1)
    		return 1;
    	else
    		return 0;
    }

    public function stampAvvisoTesserato(){
    	$frase = "non tesserat";
    	if($this->genInfo["sesso"] == "F")
    		$frase .= "a";
    	else
    		$frase .= "o";
    	$frase .= " per la stagione ".date("Y");
    	return $frase;
    }

    //personal best
    //TODO: scrive la funzione in maniera appropriata!
    public function stampPBList(){

    	$stringa = '';
    	if($this->personalBest && count($this->personalBest)>0){
    		$stringa .= '<ul id="pb_ul">';
	    	$num  = $numero_pb_ufficiali = 0;
	    	$more = false;
	    	$classe = "";
	    	foreach ( $this->personalBest as $pb ) {
	    		if(	$pb['ufficiale']=='1' ||
	    			($numero_pb_ufficiali < 2) ){
	    			if($num==4){
	    				$more 	= true;
	    				$classe = "class=\"s-more\"";
	    			}
	    			$stringa.='<li '.$classe.'>'.$pb['gara'].': <span class="orange">'.$pb['prestazione'].'</span> <span class="gray">- '.$pb['localita'].'</span></li>';
	    			$num=$num+1;

	    			if($pb['ufficiale']=='1'){
	    				$numero_pb_ufficiali++;
	    			}
		        }
	    	}
	        if($more){
	          $stringa.='<li id="a-more"><a href="#info_base">'.SHOW_MORE.'...</a></li>';
	        }
	        $stringa .= '</ul>';
	  	}
	  	else{
	  		$stringa = '<p class="pb-error">'.NO_UFFICIAL_COMPETITION.'</p>';
	  	}

    	return $stringa;
    }

    public function getPBList(){
    	return $this->personalBest;
    }

    public function stampListGareJSON(){
    	// mi occupo di stampare in JSON i nomi delle gare
    	return json_encode($this->listGare);
    }

    // TODO: valutare nome
    public function stampGeneralInfo(){
    	return $this->genInfo;
    }

    public function stampFirstGara(){
    	if($this->listGare && count($this->listGare)>0)
    		return $this->listGare[0]["gara"];
    	else
    		return null;
    }

}


#SOCIETA
class Club {
	var $ID;
	var $genInfo      = null;
	var $listAthletes = null;

	public function __construct( $ID ){
		$this->ID = $ID;
    }

	public function getGeneralInfo(){	
		require( ROOT_DIR."/queries.php" );
    	$query = new queryHandler( "django" );
    	$array = array(
					":id" => $this->ID
				);
		$query -> runQuery( $q_select_info_club, $array );
		$result = $query -> getResult();

		if($result && count($result)>0){
			$this->genInfo 			= $result[0];
			return true;
		}
		return false;
	}

	public function getID(){
		return $this->ID;
	}

	public function getAllAthletes(){
		$this->listAthletes = new ListAthletes();
		$this->listAthletes->getAthleteFromClub( $this->ID );
	}

	public function getNome(){
		return $this->genInfo["nome_societa"];
	}

	public function getLocalita(){
		return $this->genInfo["localita"];
	}

	public function getCodiceProvincia(){
		return $this->genInfo["codice_provincia"];
	}

	public function getEmail(){
		return $this->genInfo["codice"]."@fidal.it";
	}

	public function getNumeroAtleti(){
		return 110;
	}

	public function sortListAthletes( $parameter ){
		$this->listAthletes->sort( $parameter );
	}

	public function getListAthletes(){
		return $this->listAthletes->getListAthletes();
	}

	public function stampListAthletesJSON(){
		return json_encode($this->listAthletes->getOnlyInfoAthletes());
	}

	public function getMapPhoto( $dim_photo_x, $dim_photo_y ){
    	if( $this->genInfo["localita"]!=null &&
    	    $this->genInfo["codice_provincia"]!=null ){
    		//carico la foto da google maps
    		$localita = str_replace(" ", "+", $this->genInfo["localita"]);
    		return 'https://maps.googleapis.com/maps/api/staticmap?center='.$localita.','.$this->genInfo["codice_provincia"].'&zoom=11&size='.$dim_photo_x.'x'.$dim_photo_y.'&key=AIzaSyAMJjVXCDC8pT8wUnuzRbBRxKBJjt0GayU';
    	}
    	else{
    		return "";
    	}
	}


	//records
	public function thereAreRecords(){	
		require( ROOT_DIR."/queries.php" );
    	$query = new queryHandler( "django" );
    	$array = array(
					":societa_id" => $this->ID
				);
		$query -> runQuery( $q_club_record_current_year, $array );
		$result = $query -> getResult();

		if($result && count($result)>0){
			$this->allRecords = $result;
			return true;
		}
		return false;
	}

	public function getRecordFromGender( $gender ){
		$tempRecord    = array();
		foreach ($this->allRecords as $record) {
			if( $record["gender"] == $gender ){
				$record["best"] = $this->convertToTime( $record["best"] );
				array_push($tempRecord, $record);
			}
		}

		return $tempRecord;
	}

	private function convertToTime( $secT ){
		$sec_num = (int)$secT;
		$decimi=$secT*100-$sec_num*100;
		$decimi=(int)$decimi;
		$hours   = floor($sec_num / 3600);
		$minutes = floor(($sec_num - ($hours * 3600)) / 60);
		$seconds = $sec_num - ($hours * 3600) - ($minutes * 60);
		$time="";

		if($decimi < 10){
			$decimi = "0".$decimi;
		}

		if($minutes==0){
			$time=$seconds.".".$decimi;
		}
		else{
			if($seconds < 10) {$seconds = "0".$seconds;}
			if($hours==0){
				$time=$minutes.':'.$seconds.".".$decimi;
			}
			else{
				if($minutes < 10) {$minutes = "0".$minutes;}
					$time    = $hours.'h'.$minutes.':'.$seconds.".".$decimi;
			}
		}
		return $time;
	}
}


class ListAthletes {
	var $listAthletes    = array();

	public function __construct(){
    }

	public function getAthleteFromClub( $ID ){
		require( ROOT_DIR."/queries.php" );

		$q     = new queryHandler( "django" );
		$array = array(
					":id" => $ID
				 );

		$q -> runQuery( $q_select_all_athletes_from_club, $array );
		$results = $q -> getResult();

		$this->caricaListAthletes( $results );
	}

	public function getRandomAthletes(){
		require( ROOT_DIR."/queries.php" );

		$q =  new queryHandler( "django" );
		$q -> runQuery( $q_select_random_athlete, [] );
		$results = $q -> getResult();

		$this->caricaListAthletes( $results );
	}

	private function caricaListAthletes( $results ){
		foreach ($results as $athlete_info) {
			$athlete = new Atleta( $athlete_info["id"] );
			$athlete -> setGeneralInfo( $athlete_info );

			array_push( $this->listAthletes, $athlete );
		}
	}

	public function getListAthletes(){
		return $this->listAthletes;
	}

	public function getOnlyInfoAthletes(){
		$array = array();
		foreach ($this->listAthletes as $athlete) {
			array_push($array, $athlete->stampGeneralInfo());
		};

		return $array;
	}

	public function sort( $parameter ){
		if( $parameter == 'categoria' )
			usort( $this->listAthletes, array('AthleteSorter', 'sortByCategoriaChampion') );
		else if( $parameter == 'specialita' )
			usort( $this->listAthletes, array('AthleteSorter', 'sortBySpecialitaChampion') );
		else if( $parameter == 'alfabeto' )
			usort( $this->listAthletes, array('AthleteSorter', 'sortByName') );
	}

}


class AthleteSorter {

	public static function sortByName($a, $b) {
	    if ($a->getNomeCognome() == $b->getNomeCognome()) {
	        return 0;
	    }
	    return ($a->getNomeCognome() < $b->getNomeCognome()) ? -1 : 1;
	}


	//sort atleti in societa
	public static function sortByCategoriaChampion($a, $b) {
	    if ($a->getAttribute("categoria") == $b->getAttribute("categoria")) {
	        return self::sortByChampion($a, $b);
	    }
	    return ($a->getAttribute("categoria") < $b->getAttribute("categoria")) ? 1 : -1;
	}

	public static function sortBySpecialitaChampion($a, $b) {
	    if ($a->getAttribute("specialita") == $b->getAttribute("specialita")) {
	        return self::sortByChampion($a, $b);
	    }
	    return ($a->getAttribute("specialita") < $b->getAttribute("specialita")) ? 1 : -1;
	}

	public static function sortByFirstLetterChampion($a, $b){
		$a_nome = $a->getNomeCognome();
		$b_nome = $b->getNomeCognome();

		if ($a_nome[0] == $b_nome[0]) {
	        return self::sortByChampion($a, $b);
	    }
	    return ($a_nome[0] < $b_nome[0]) ? -1 : 1;
	}

	public static function sortByChampion($a, $b) {
	    if ($a->getAttribute("punteggio_migliore_anno_attuale") == $b->getAttribute("punteggio_migliore_anno_attuale")) {
	        return 0;
	    }
	    return ($a->getAttribute("punteggio_migliore_anno_attuale") < $b->getAttribute("punteggio_migliore_anno_attuale")) ? -1 : 1;
	}

}



class Validator {

	public function isRegione( $regione ){
		$regioni=array(" ", "Abruzzo","Basilicata","Calabria","Campania","Emilia-Romagna","Friuli-Venezia Giulia","Lazio","Liguria","Lombardia","Marche","Molise","Piemonte", "Puglia", "Sardegna","Sicilia","Toscana","Trentino-Alto Adige","Umbria","Valle d'Aosta","Veneto");
		return in_array( $regione, $regioni );
	}
}

?>