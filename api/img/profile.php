<?php
header('Content-type: image/png');

$im = imagecreatetruecolor(2000, 2000);

// sets background to red
$red = imagecolorallocate($im, 255, 0, 0);
imagefill($im, 0, 0, $red);

imagejpeg($im);

imagedestroy($im);

?>