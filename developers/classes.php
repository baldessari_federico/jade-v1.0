<?php	

#GESTORE DELLE PAGINE
class Page {
    var $namePage;

	public function __construct( $name ){
		$this->namePage = $name;
    }

	public function render( $values ){

		try{
			require_once( 'libs/Twig/Autoloader.php' );
			Twig_Autoloader::register();
			$loader   = new Twig_Loader_Filesystem( ROOT_DIR.'/templates' );
			$twig     = new Twig_Environment( $loader );
			$template = $twig->loadTemplate( $this->namePage );

			$html_script = new HtmlConverter(CODE_LIBRARY);
			$values['Script'] = $html_script;
			echo $template->render( $values );

		} catch( Exception $e ) {
			die( ERROR.": ".$e->getMessage() );
		}

	}
}

class HtmlConverter {
	var $code;

	public function __construct( $code ){
		$this->code = $code;
    }

    public function htmlCode(){
    	$htmlCode = htmlentities($this->code);
		return $htmlCode;
	}

	public function normCode(){
		return $this->code;
	}

	public function insideHTML( $precode ){
		$html_template = '	ZI<!DOCTYPE html>ZF
							ZI<html>ZF
							ZI<head>ZF
								ZI TT<title>Prova JADE - atletica.me</title>ZF
								ZI TT<script rel="javascript" src="http://api.atletica.me/script/jade.js"></script>ZF
							ZI</head>ZF
							ZI<body>ZF
								ZI TT <!-- il tuo codice --> ZF
								ZI TT '.$precode.': ZF
								ZI TT <!-- posiziona l\'elemento dove preferisci --> ZF
								ZI TT'.$this->code.' ZF
							ZI</body>ZF
							ZI</html>ZF';

		$html_template = str_replace(
							"TT", "<span class=\"ident\"></span>",
							str_replace(
								"ZF", "<p class=\"paddTB2\">",
								str_replace(
									"ZI", "</p>",
									htmlentities($html_template)
								)
							)
						);
		return $html_template;
	}

	public function blankPage(){
		$html_template = '	ZI<!DOCTYPE html>ZF
							ZI<html>ZF
							ZI<head>ZF
								ZI TT<title>Prova JADE - atletica.me</title>ZF
								ZI TT<script rel="javascript" src="http://api.atletica.me/script/jade.js"></script>ZF
							ZI</head>ZF
							ZI<body>ZF
								ZI TT <!-- il tuo codice --> ZF
							ZI</body>ZF
							ZI</html>ZF';

		$html_template = str_replace(
							"TT", "<span class=\"ident\"></span>",
							str_replace(
								"ZF", "<p class=\"paddTB2\">",
								str_replace(
									"ZI", "</p>",
									htmlentities($html_template)
								)
							)
						);
		return $html_template;
	}

	public function insertScriptWP(){
		$html_template = '	ZI<!DOCTYPE html>ZF
							ZI<html>ZF
							ZI<head>ZF
								ZI TT[ ... ]ZF
								

								ZI TT<script rel="javascript" src="http://api.atletica.me/script/jade.js"></script>ZF

								ZI TT[ ... ]ZF

							ZI</head>ZF
							ZI<body>ZF';

		$html_template = str_replace(
							"TT", "<span class=\"ident\"></span>",
							str_replace(
								"ZF", "<p class=\"paddTB2\">",
								str_replace(
									"ZI", "</p>",
									htmlentities($html_template)
								)
							)
						);
		return $html_template;
	}
}

class dateConverter{
	var $data;

	public function __construct( $data ){
		$this->data = $data;
    }
}


?>