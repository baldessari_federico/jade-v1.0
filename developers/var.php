<?php

//TUTTE LE OPZIONI DISPONIBILI NELLE VARIE PAGINE

//BADGE_ATLETA
$O_BADGE_ATLETA = array(
	0 => array(
		"NAME"  		=> 'url_athlete',
		"DESCRIPTION" 	=> DESC_URL_ATHLETE,
		"LABEL" 		=> URL_ATHLETE,
		"VALUE" => '<form action="javascript:void(0)" onsubmit="modifyCodeFromNum(this, \'data-attr\');">
						<input type="text" value="http://atletica.me/atleta/Giulia-Rossi/700">
					</form>'
		),
	1 => array(
		"NAME"  		=> 'width',
		"DESCRIPTION" 	=> DESC_WIDTH,
		"LABEL" 		=> WIDTH,
		"VALUE" => '<select data-attr="width" class="changer">
						<option value="">'.			NORMAL		.'</option>
						<option value="minBadge">'.	MINIATURE	.'</option>
					</select>'
		)
);

//BADGE_ATLETA_ESTESO
$O_BADGE_ATLETA_ESTESO = array(
	0 => array(
		"NAME"  		=> 'url_athlete',
		"DESCRIPTION" 	=> DESC_URL_ATHLETE,
		"LABEL" 		=> URL_ATHLETE,
		"VALUE" => '<form action="javascript:void(0)" onsubmit="modifyCodeFromNum(this, \'data-attr\');">
						<input type="text" value="http://atletica.me/atleta/Giulia-Rossi/700">
					</form>'
		)
);

//ATLETA_PB
$O_ATLETA_PB = array(
	0 => array(
		"NAME"  		=> 'url_athlete',
		"DESCRIPTION" 	=> DESC_URL_ATHLETE,
		"LABEL" 		=> URL_ATHLETE,
		"VALUE" => '<form action="javascript:void(0)" onsubmit="modifyCodeFromNum(this, \'data-attr\');">
						<input type="text" value="http://atletica.me/atleta/Giulia-Rossi/700">
					</form>'
		)
);

//ATLETA_CARRIERA
$O_ATLETA_CARRIERA = array(
	0 => array(
		"NAME"  		=> 'url_athlete',
		"DESCRIPTION" 	=> DESC_URL_ATHLETE,
		"LABEL" 		=> URL_ATHLETE,
		"VALUE" => '<form action="javascript:void(0)" onsubmit="modifyCodeFromNum(this, \'data-attr\');">
						<input type="text" value="http://atletica.me/atleta/Giulia-Rossi/700">
					</form>'
		)
);

//ATLETA_COLLEGAMENTO
$O_ATLETA_COLLEGAMENTO = array(

);


//CLUB
$O_CLUB_BADGE = array(
	0 => array(
		"NAME"  		=> 'url_club',
		"DESCRIPTION" 	=> DESC_URL_CLUB,
		"LABEL" 		=> URL_CLUB,
		"VALUE" => '<form action="javascript:void(0)" onsubmit="modifyCodeFromNum(this, \'data-attr\');">
						<input type="text" value="http://atletica.me/societa/1800">
					</form>'
		)
);

//CLUB LISTA ATLETI
//todo: cambiare value a divider!
$O_CLUB_ATHLETES_LIST = array(
	0 => array(
		"NAME"  		=> 'url_club',
		"DESCRIPTION" 	=> DESC_URL_CLUB,
		"LABEL" 		=> URL_CLUB,
		"VALUE" => '<form action="javascript:void(0)" onsubmit="modifyCodeFromNum(this, \'data-attr\');">
						<input type="text" value="http://atletica.me/societa/1649">
					</form>'
		),
	1 => array(
		"NAME"  		=> 'divider',
		"DESCRIPTION" 	=> DESC_DIVIDER_CLUB,
		"LABEL" 		=> DIVIDER_CLUB,
		"VALUE" => '<select data-attr="divider" class="changer">
						<option value="specialita">Specialità</option>
						<option value="categoria">Categoria</option>
						<option value="alfabeto">Nome e cognome</option>
					</select>'
		)
);

//CLUB RECORD CURRENT YEAR
$O_CLUB_RECORDS_CURR_YEAR = array(
	0 => array(
		"NAME"  		=> 'url_club',
		"DESCRIPTION" 	=> DESC_URL_CLUB,
		"LABEL" 		=> URL_CLUB,
		"VALUE" => '<form action="javascript:void(0)" onsubmit="modifyCodeFromNum(this, \'data-attr\');">
						<input type="text" value="http://atletica.me/societa/1800">
					</form>'
		)
);


//MANIFESTAZIONI
//RICERCA MANIFESTAZIONI
$O_MEETINGS_SEARCH = array(
	0 => array(
		"NAME"  		=> 'region',
		"DESCRIPTION" 	=> DESC_DEFAULT_REGION,
		"LABEL" 		=> DEFAULT_REGION,
		"VALUE" => '<select data-attr="default" class="changer">
						<option value="Abruzzo">'.				ABRUZZO					.'</option>
						<option value="Basilicata">'.			BASILICATA				.'</option>
						<option value="Calabria">'.				CALABRIA				.'</option>
						<option value="Campania">'.				CAMPANIA				.'</option>
						<option value="Emilia-Romagna">'.		EMILIA_ROMAGNA			.'</option>
						<option value="Friuli-Venezia Giulia">'.FRIULI_VENEZIA_GIULIA	.'</option>
						<option value="Lazio">'.				LAZIO					.'</option>
						<option value="Liguria">'.				LIGURIA					.'</option>
						<option value="Lombardia">'.			LOMBARDIA				.'</option>
						<option value="Marche">'.				MARCHE					.'</option>
						<option value="Molise">'.				MOLISE					.'</option>
						<option value="Piemonte">'.				PIEMONTE				.'</option>
						<option value="Puglia">'.				PUGLIA					.'</option>
						<option value="Sardegna">'.				SARDEGNA				.'</option>
						<option value="Sicilia">'.				SICILIA					.'</option>
						<option value="Toscana">'.				TOSCANA					.'</option>
						<option value="Trentino-Alto Adige">'.	TRENTINO_ALTO_ADIGE		.'</option>
						<option value="Umbria">'.				UMBRIA					.'</option>
						<option value="Valle d\'Aosta">'.		VALLE_D_AOSTA			.'</option>
						<option value="Veneto">'.				VENETO					.'</option>		
					</select>'
		)
);

//RISULTATI MEETING IN REAL TIME
$O_MEETING_RESULTS_REAL_TIME = $O_MEETINGS_SEARCH;

?>