<?php

	require_once( 'libs/glue.php' );
	require_once( 'pages.php' );

	$urls = array(

		// JADE
		'/JADE/v1.0(/)?' 						=> 	'show_index',
		'/JADE/v1.0/badge-atleta(/)?' 			=> 	'show_badge_atleta',
		'/JADE/v1.0/badge-atleta-esteso(/)?' 	=> 	'show_badge_atleta_esteso',
		'/JADE/v1.0/atleta-pb(/)?' 				=> 	'show_atleta_pb',
		'/JADE/v1.0/atleta-carriera(/)?' 		=> 	'show_atleta_carriera',
		'/JADE/v1.0/atleta-collegamento(/)?' 	=> 	'show_atleta_collegamento',
		'/JADE/v1.0/badge-societa(/)?' 			=> 	'show_club_badge',
		'/JADE/v1.0/societa-lista-atleti(/)?' 	=> 	'show_club_athletes_list',
		'/JADE/v1.0/societa-record-correnti(/)?'=>  'show_club_records_curr_year',
		'/JADE/v1.0/gare-manifestazioni(/)?' 	=>  'show_meetings_search',
		'/JADE/v1.0/gare-risultati-real-time(/)?'=>  'show_meeting_results_real_time',

		// IDs
		'/example/number/(\d+)' 			=> 	'show_index',
		'/example/string/(PER[0-9]+)'		=> 	'show_number_or_string',

		// ACTIONS
		'/example/insert_item'				=> 	'insert_item'

	);
	
	glue::stick($urls);
?>