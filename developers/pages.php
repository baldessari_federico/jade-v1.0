<?php

require( "classes.php" );
require_once( 'config.php' );

// PAGINE JADE

// introduzione
class show_index {
	function GET() {

		$values   = array(
			'title'        => TITLE_INTRO,
			'subtitle'     => SUBTITLE_INTRO,
			'description'  => DESC_INTRO,
			'post_desc'	   => POST_DESC_INTRO,
		);

		$page = new Page("show");
		$page -> render( $values );
	}
}

class show_badge_atleta {
	function GET() {
		require( 'var.php' );
		$html_conv = new HtmlConverter(CODE_BADGE_ATLETA);

		$options = $O_BADGE_ATLETA;

		$values   = array(
			'title'        => TITLE_BADGE_ATLETA,
			'subtitle'     => SUBTITLE_BADGE_ATLETA,
			'description'  => DESC_BADGE_ATLETA,
			'precode'	   => PRE_CODE_BADGE_ATLETA,
			'options'      => $options,
			'Code'    	   => $html_conv
		);

		$page = new Page("show");
		$page -> render( $values );
	}
}

class show_badge_atleta_esteso {
	function GET() {
		require( 'var.php' );
		$html_conv = new HtmlConverter(CODE_BADGE_ATLETA_ESTESO);

		$options = $O_BADGE_ATLETA_ESTESO;

		$values   = array(
			'title'        => TITLE_BADGE_ATLETA_ESTESO,
			'subtitle'     => SUBTITLE_BADGE_ATLETA_ESTESO,
			'description'  => DESC_BADGE_ATLETA_ESTESO,
			'precode'	   => PRE_CODE_BADGE_ATLETA_ESTESO,
			'options'      => $options,
			'Code'    	   => $html_conv
		);

		$page = new Page("show");
		$page -> render( $values );
	}
}

class show_atleta_pb {
	function GET() {
		require( 'var.php' );
		$html_conv = new HtmlConverter(CODE_ATLETA_PB);

		$options = $O_ATLETA_PB;

		$values   = array(
			'title'        => TITLE_ATLETA_PB,
			'subtitle'     => SUBTITLE_ATLETA_PB,
			'description'  => DESC_ATLETA_PB,
			'precode'	   => PRE_CODE_ATLETA_PB,
			'options'      => $options,
			'Code'    	   => $html_conv
		);

		$page = new Page("show");
		$page -> render( $values );
	}
}

class show_atleta_carriera {
	function GET() {
		require( 'var.php' );
		$html_conv = new HtmlConverter(CODE_ATLETA_CARRIERA);

		$options = $O_ATLETA_CARRIERA;

		$values   = array(
			'title'        => TITLE_ATLETA_CARRIERA,
			'subtitle'     => SUBTITLE_ATLETA_CARRIERA,
			'description'  => DESC_ATLETA_CARRIERA,
			'precode'	   => PRE_CODE_ATLETA_CARRIERA,
			'options'      => $options,
			'Code'    	   => $html_conv
		);

		$page = new Page("show");
		$page -> render( $values );
	}
}

class show_atleta_collegamento {
	function GET() {
		require( 'var.php' );
		$html_conv = new HtmlConverter(CODE_ATLETA_COLLEGAMENTO);

		$options = $O_ATLETA_COLLEGAMENTO;

		$values   = array(
			'title'        => TITLE_ATLETA_COLLEGAMENTO,
			'subtitle'     => SUBTITLE_ATLETA_COLLEGAMENTO,
			'description'  => DESC_ATLETA_COLLEGAMENTO,
			'precode'	   => PRE_CODE_ATLETA_COLLEGAMENTO,
			'options'      => $options,
			'Code'    	   => $html_conv
		);

		$page = new Page("show");
		$page -> render( $values );
	}
}

class show_club_badge {
	function GET() {
		require( 'var.php' );
		$html_conv = new HtmlConverter(CODE_CLUB_BADGE);

		$options = $O_CLUB_BADGE;

		$values   = array(
			'title'        => TITLE_CLUB_BADGE,
			'subtitle'     => SUBTITLE_CLUB_BADGE,
			'description'  => DESC_CLUB_BADGE,
			'precode'	   => PRE_CODE_CLUB_BADGE,
			'options'      => $options,
			'Code'    	   => $html_conv
		);

		$page = new Page("show");
		$page -> render( $values );
	}
}

class show_club_athletes_list {
	function GET() {
		require( 'var.php' );
		$html_conv = new HtmlConverter(CODE_CLUB_ATHLETES_LIST);

		$options = $O_CLUB_ATHLETES_LIST;

		$values   = array(
			'title'        => TITLE_CLUB_ATHLETES_LIST,
			'subtitle'     => SUBTITLE_CLUB_ATHLETES_LIST,
			'description'  => DESC_CLUB_ATHLETES_LIST,
			'precode'	   => PRE_CODE_CLUB_ATHLETES_LIST,
			'options'      => $options,
			'Code'    	   => $html_conv
		);

		$page = new Page("show");
		$page -> render( $values );
	}
}

class show_club_records_curr_year {
	function GET() {
		require( 'var.php' );
		$html_conv = new HtmlConverter(CODE_CLUB_RECORDS_CURR_YEAR);

		$options = $O_CLUB_RECORDS_CURR_YEAR;

		$values   = array(
			'title'        => TITLE_CLUB_RECORDS_CURR_YEAR,
			'subtitle'     => SUBTITLE_CLUB_RECORDS_CURR_YEAR,
			'description'  => DESC_CLUB_RECORDS_CURR_YEAR,
			'precode'	   => PRE_CODE_CLUB_RECORDS_CURR_YEAR,
			'options'      => $options,
			'Code'    	   => $html_conv
		);

		$page = new Page("show");
		$page -> render( $values );
	}
}

class show_meetings_search {
	function GET() {
		require( 'var.php' );
		$html_conv = new HtmlConverter(CODE_MEETINGS_SEARCH);

		$options = $O_MEETINGS_SEARCH;

		$values   = array(
			'title'        => TITLE_MEETINGS_SEARCH,
			'subtitle'     => SUBTITLE_MEETINGS_SEARCH,
			'description'  => DESC_MEETINGS_SEARCH,
			'precode'	   => PRE_CODE_MEETINGS_SEARCH,
			'options'      => $options,
			'Code'    	   => $html_conv
		);

		$page = new Page("show");
		$page -> render( $values );
	}
}

class show_meeting_results_real_time {
	function GET() {
		require( 'var.php' );
		$html_conv = new HtmlConverter(CODE_MEETING_RESULTS_REAL_TIME);

		$options = $O_MEETING_RESULTS_REAL_TIME;

		$values   = array(
			'title'        => TITLE_MEETING_RESULTS_REAL_TIME,
			'subtitle'     => SUBTITLE_MEETING_RESULTS_REAL_TIME,
			'description'  => DESC_MEETING_RESULTS_REAL_TIME,
			'precode'	   => PRE_CODE_MEETING_RESULTS_REAL_TIME,
			'options'      => $options,
			'Code'    	   => $html_conv
		);

		$page = new Page("show");
		$page -> render( $values );
	}
}

?>