<?php

	function isFirstAccess() {
		return !isset( $_SESSION["lang"] );
	}

	function getSystemLanguage() {
		return substr( $_SERVER["HTTP_ACCEPT_LANGUAGE"], 0, 2 );
	}

	function languageIsUnsupported() {
		return ( $_SESSION["lang"] != "it" ) && ( $_SESSION["lang"] != "en" ) && ( $_SESSION["lang"] != "de" ) && ( $_SESSION["lang"] != "fr" );
	}


	// SET LANGUAGE
	if ( isFirstAccess() )
		$_SESSION["lang"] = getSystemLanguage();

	if ( $_SESSION["lang"] == "it" ) {
		require( ROOT_DIR."/languages/it_it.php" );
	} else {
		//attualmente il dizionario inglese non è ancora pronto quindi mostro italiano
		//require( ROOT_DIR."/languages/en_US.php" );
		require( ROOT_DIR."/languages/it_it.php" );
	}

?>