<?php

	// COMMON
	define("PROJECT_NAME", 	"Example");
	define("INDEX", 		"HomePage");
	define("HIDDEN", 		"Nascosto");
	define("VISIBLE", 		"Visibile");
	define("SUBMIT", 		"Invia");

	//mesi
	define("MONTH_1", 					"Gennaio");
	define("MONTH_2", 					"Febbraio");
	define("MONTH_3", 					"Marzo");
	define("MONTH_4", 					"Aprile");
	define("MONTH_5", 					"Maggio");
	define("MONTH_6", 					"Giugno");
	define("MONTH_7", 					"Luglio");
	define("MONTH_8", 					"Agosto");
	define("MONTH_9", 					"Settembre");
	define("MONTH_10", 					"Ottobre");
	define("MONTH_11", 					"Novebre");
	define("MONTH_12", 					"Dicembre");


	//regioni
	define("ABRUZZO", 					"Abruzzo");
	define("BASILICATA", 				"Basilicata");
	define("CALABRIA", 					"Calabria");
	define("CAMPANIA", 					"Campania");
	define("EMILIA_ROMAGNA", 			"Emilia-Romagna");
	define("FRIULI_VENEZIA_GIULIA", 	"Friuli-Venezia Giulia");
	define("LAZIO", 					"Lazio");
	define("LIGURIA", 					"Liguria");
	define("LOMBARDIA", 				"Lombardia");
	define("MARCHE", 					"Marche");
	define("MOLISE", 					"Molise");
	define("PIEMONTE", 					"Piemonte");
	define("PUGLIA", 					"Puglia");
	define("SARDEGNA", 					"Sardegna");
	define("SICILIA", 					"Sicilia");
	define("TOSCANA", 					"Toscana");
	define("TRENTINO_ALTO_ADIGE", 		"Trentino-Alto Adige");
	define("UMBRIA", 					"Umbria");
	define("VALLE_D_AOSTA", 			"Valle d'Aosta");
	define("VENETO", 					"Veneto");

	define("ALL_REGIONS", 				"Tutta italia");

	// LABEL
	define("URL_ATHLETE", 			"Url Atleta");
	define("URL_CLUB", 				"Url Societa");
	define("WIDTH", 				"Ampiezza");
	define("COLOR", 				"Colore");
	define("DIVIDER_CLUB", 			"Dividi per");
	define("DEFAULT_REGION", 		"Regione Standard");

	// DESCRIZIONE GENERALE OPZIONI
	define("DESC_URL_ATHLETE", 		"Copia e incolla il link dell'atleta che ti interessa preso da Atletica.me");
	define("DESC_URL_CLUB", 		"Copia e incolla il link della societa che ti interessa preso da Atletica.me");
	define("DESC_WIDTH", 			"Scegli le dimensioni che preferisci");
	define("DESC_COLOR", 			"E' possibile trovare....");
	define("DESC_DIVIDER_CLUB", 	"Dividi gli atleti per una lettura facilitata che permetta agli utenti di trovare gli atleti rapidamente");
	define("DESC_DEFAULT_REGION", 	"Seleziona una regione dalla quale far partire la ricerca. L'utente potrà facilmente cambiare questa regione ma è buona norma far partire la ricerca da una regione della quale ci si aspetta l'utente medio possa essere interessato. Se ti aspetti che visitino il tuo sito principalmente persone del Veneto allora fai partire la ricerca dal Veneto o Lombardia");

	// INTRODUZIONE
	define("TITLE_INTRO", 		"INTRODUZIONE");
	define("SUBTITLE_INTRO", 	"Come iniziare");
	define("DESC_INTRO", 		"JADE è la prima libreria interamente pensata per l'atletica in Italia.</br> Includi in pochi passi le informazioni che ti interessano all'interno del tuo sito e lascia il compito a JADE di aggiornarle continuamente.</br> Tramite l'accesso al database di Atletica.me avrai a disposizione tutti i dati su più di 300mila atleti e 5mila società automaticamente aggiornati alle ultime modifiche e pronti per essere utilizzati sul tuo blog o sito personale.</br> JADE è aperto a tutti e non necessita di registrazione. Leggi la semplice documentazione e inizia subito a risparmiare tempo e denaro");
	define("POST_DESC_INTRO", 	"Inizia includendo la libreria all'interno del tuo sito");



	// ATLETA
	define("TITLE_BADGE_ATLETA", 	"BADGE ATLETA");
	define("SUBTITLE_BADGE_ATLETA", "La tua carriera sportiva");
	define("DESC_BADGE_ATLETA", 	"Con Badge Atleta puoi facilmente integrare il profilo di un'atleta all'interno del tuo sito. Utilizza la versione estesa per poter consultare anche i personal best oppure la versione ridotta per avere una visione più immediata.");
	define("PRE_CODE_BADGE_ATLETA",	"Eletto miglior atleta");

	// ATLETA ESTESO
	define("TITLE_BADGE_ATLETA_ESTESO", 	TITLE_BADGE_ATLETA);
	define("SUBTITLE_BADGE_ATLETA_ESTESO", 	SUBTITLE_BADGE_ATLETA);
	define("DESC_BADGE_ATLETA_ESTESO", 		DESC_BADGE_ATLETA);
	define("PRE_CODE_BADGE_ATLETA_ESTESO", 	PRE_CODE_BADGE_ATLETA);

	// ATLETA PB
	define("TITLE_ATLETA_PB", 		"Atleta PB");
	define("SUBTITLE_ATLETA_PB", 	"Personal Best");
	define("DESC_ATLETA_PB", 		"Carica solo i personal best di un atleta all'interno del tuo sito. Saranno sempre aggiornati in tempo reale. Ricorda che se vuoi caricare anche le informazioni sull'atleta devi utilizzare Badge Atleta Esteso");
	define("PRE_CODE_ATLETA_PB", 	"I personal best di Nome Cognome sono");

	// ATLETA CARRIERA
	define("TITLE_ATLETA_CARRIERA", 	"Carriera Atleta");
	define("SUBTITLE_ATLETA_CARRIERA", 	"Tutti i risultati in un click");
	define("DESC_ATLETA_CARRIERA", 		"Carica tutti i risultati di un atleta all'interno del tuo sito. Saranno sempre aggiornati in tempo reale. Ricorda che per caricare le informazioni sull'atleta devi utilizzare Badge Atleta");
	define("PRE_CODE_ATLETA_CARRIERA", 	"Tutti i risultati di Nome Cognome");

	// ATLETA COLLEGAMENTO
	define("TITLE_ATLETA_COLLEGAMENTO", 	"Collegamento Atleta");
	define("SUBTITLE_ATLETA_COLLEGAMENTO", 	"Un link potentissimo");
	define("DESC_ATLETA_COLLEGAMENTO", 		"Tutta la potenza di JADE in un semplice link. Non è mai stato così facile e veloce inserire nel proprio sito così tante informazioni. Devi solo inserire il collegamento all'interno del tuo sito dove preferisci e penseremo a tutto noi");
	define("PRE_CODE_ATLETA_COLLEGAMENTO", 	"Grandissima prestazione di ");



	//SOCIETA

	// BADGE SOCIETA
	define("TITLE_CLUB_BADGE", 		"Badge Societa");
	define("SUBTITLE_CLUB_BADGE", 	"Info generali");
	define("DESC_CLUB_BADGE", 		"Con Badge Societa puoi facilmente integrare le informazioni di una società sportiva all'interno del tuo sito. Le informazioni sul numero di atleti, dirigenza ecc saranno automaticamente aggiornate e non dovrei più preoccuparti di scrivere nulla");
	define("PRE_CODE_CLUB_BADGE", 	"Ecco la società che ha vinto i CDS");

	// LISTA ATLETI SOCIETA
	define("TITLE_CLUB_ATHLETES_LIST", 		"Lista Atleti Societa");
	define("SUBTITLE_CLUB_ATHLETES_LIST", 	"Tutti gli atleti");
	define("DESC_CLUB_ATHLETES_LIST", 		"Ideale per la creazione di una pagina in cui mostrare tutti gli atleti della propria società. Lista Atleti Società permette inoltre di dividere gli atleti per nome, categoria o specialità per permettere una ricerca più rapida oltre a fornire un semplice strumento di ricerca. Grazie a questa funzione non dovrete più aggiornare la lista degli atleti ma sarà completamente automatizzata");
	define("PRE_CODE_CLUB_ATHLETES_LIST", 	"Tutti i nostri atleti");

	// RECORD SOCIETA
	define("TITLE_CLUB_RECORDS_CURR_YEAR", 		"Record Societa");
	define("SUBTITLE_CLUB_RECORDS_CURR_YEAR", 	"I nostri successi");
	define("DESC_CLUB_RECORDS_CURR_YEAR", 		"Non dovrai più aggiornare continuamente i record della tua società sportiva. D'ora in avanti pensiamo a tutto noi. Tutti i record dell'anno corrente delle gare in pista aggiornati in tempo reale");
	define("PRE_CODE_CLUB_RECORDS_CURR_YEAR", 	"I record di quest'anno della nostra società");

	//MANIFESTAZIONI - GARE
	//RICERCA MANIFESTAZIONI
	define("TITLE_MEETINGS_SEARCH", 	"Manifestazioni");
	define("SUBTITLE_MEETINGS_SEARCH", 	"Cerca una gara");
	define("DESC_MEETINGS_SEARCH", 		"Semplice widget per permettere la ricerca delle manifestazioni di atletica direttamente all'interno del proprio sito web. È possibile configurare una regione di partenza per velocizzare la ricerca");
	define("PRE_CODE_MEETINGS_SEARCH", 	"Cerca una gara:");

	//RICERCA MANIFESTAZIONI
	define("TITLE_MEETING_RESULTS_REAL_TIME", 		"Risultati");
	define("SUBTITLE_MEETING_RESULTS_REAL_TIME", 	"in tempo reale");
	define("DESC_MEETING_RESULTS_REAL_TIME", 		"I migliori risultati del weekend in italia o in una regione raggruppati in un unico posto");
	define("PRE_CODE_MEETING_RESULTS_REAL_TIME", 	"I migliori risultati degli ultimi giorni in Italia");




	//CODICE (non tradurre, va spostato in var)
	//atleta
	define("CODE_BADGE_ATLETA", 		'<div data-attr="116452" class="j-badge-atleta"></div>');
	define("CODE_BADGE_ATLETA_ESTESO", 	'<div data-attr="116452" class="j-badge-atleta-pb"></div>');
	define("CODE_ATLETA_PB", 			'<div data-attr="116452" class="j-atleta-pb"></div>');
	define("CODE_ATLETA_CARRIERA", 		'<div data-attr="116452" class="j-atleta-carriera"></div>');
	define("CODE_ATLETA_COLLEGAMENTO", 	'<a href="http://atletica.me/atleta/10">Nome Atleta</a>');

	//societa
	define("CODE_CLUB_BADGE", 				'<div data-attr="1800" class="j-societa-badge"></div>');
	define("CODE_CLUB_ATHLETES_LIST", 		'<div data-attr="1649" class="j-societa-lista-atleti"></div>');
	define("CODE_CLUB_RECORDS_CURR_YEAR", 	'<div data-attr="1800" class="j-societa-records-anno-curr"></div>');

	//manifestazioni
	define("CODE_MEETINGS_SEARCH", 				'<div class="j-gare-ricerca"></div>');
	define("CODE_MEETING_RESULTS_REAL_TIME", 	'<div class="j-risultati-tempo-reale"></div>');


	define("CODE_LIBRARY", '<script rel="javascript" src="http://api.atletica.me/script/jade.js"></script>');


	define("NORMAL", "Normale");
	define("MINIATURE", "Miniatura");
?>