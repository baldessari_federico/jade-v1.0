<?php

	// COMMON
	define("PROJECT_NAME", 	"english");
	define("INDEX", 		"english");
	define("HIDDEN", 		"english");
	define("VISIBLE", 		"english");
	define("SUBMIT", 		"english");


	// LABEL
	define("URL_ATHLETE", 	"english");
	define("WIDTH", 		"english");
	define("COLOR", 		"english");

	// DESCRIZIONE GENERALE OPZIONI
	define("DESC_URL_ATHLETE", 	"Copia e incolla il link dell'atleta che ti interessa preso da Atletica.me");
	define("DESC_WIDTH", 		"english");
	define("DESC_COLOR", 		"E' possibile trovare....");


	// INTRODUZIONE
	define("TITLE_INTRO", 		"english");
	define("SUBTITLE_INTRO", 	"english");
	define("DESC_INTRO", 		"");

	// ATLETA
	define("TITLE_BADGE_ATLETA", 	"english");
	define("SUBTITLE_BADGE_ATLETA", "english");
	define("DESC_BADGE_ATLETA", 	"Con Badge Atleta puoi facilmente integrare il profilo di un'atleta all'interno del tuo sito. Utilizza la versione estesa per poter consultare anche i personal best oppure la versione ridotta per avere una visione più immediata.");
	define("CODE_BADGE_ATLETA", '<div data-attr="116452" class="j-badge-atleta"></div>');


?>